#!/usr/bin/bash

set -o errexit
set -o pipefail
set -o nounset

usage() {
	echo "Usage: $0 what-to-do [options]

Where what-to-do can be one of:
 help          Show this help screen
 download      Download (but don't install) the sources
 dependencies  Compile all the dependencies (but not gstlal-*)
 build-spiir   Check out gstlal (usually SPIIR branch), build gstlal-*
 environment   Give you a neat function to put in your bashrc

This script is designed to help you get started with a development environment
for SPIIR[0]. There are two basic roads you might want to go down: either you
want a completely clean environment all for yourself, including all
dependencies; OR you're part of a wider group and someone else has already done
most of the set-up for you.

In the first case, you'll generally want to run the script four times,
specifying in sequence these commands: download (which downloads everything
except gstlal), dependencies (which builds all of the dependencies, again
except for gstlal-*), build-spiir (which checks out the gstlal git repository,
and builds gstlal, gstlal-inspiral, gstlal-ugly and gstlal-spiir), and
environment (which helps you set up your .bashrc).

In the second case (part of a wider group), generally you'll want to run just
one command. If you just want to use SPIIR (i.e., need access to the libraries
so you can 'import' them into Python scripts etc), you'll just need to set up
your environment. If you want to develop SPIIR itself, you'll generally want to
run build-spiir and then environment, so you have your own git repository and
your own installation for testing.

The script figures out where to put things (e.g. installation, source files)
either based on where you tell it, or (if it can) by magically guessing. This
magical guessing is based on hard-coded paths for certain use-cases; at the
moment the only supported magical guessing case is the UWA SPIIR group.


Below is a detailed description of what each of the 'what-to-do' choices does.

help
  Show this help screen.

download
  Download the sources for all the dependencies. Some of these are tarballs
  that are simply downloaded with wget (they won't be re-downloaded if they
  already exist, although no checking is done to test archive integrity).
  Others are cloned git repositories. If the relevant dependency has already
  been cloned (tested based on directory existence) no attempt is made to
  re-download it. All sources are downloaded to --download-dir.

dependencies
  Build and install all dependencies. The sources are expected to be in the
  location specified by --download-dir; the dependencies are installed to the
  location specified by --dep-prefix. At the moment, all dependencies are
  rebuilt and reinstalled unconditionally. The build is done in --tmpdir.

build-spiir
  Check out spiir, build, and install. The dependencies are expected to have
  been installed already to --dep-prefix. The gstlal repository is cloned to
  --spiir-dir/[name]/src and installed to --spiir-dir/[name]/install, where
  [name] is given by --name. After cloning the repository, the script will
  check out the branch specified by --branch. (Note that the build is done in
  the source directory, not --tmpdir, so that it can be quickly recompiled.) 

environment
  Print out a shell function that can be used to manage the environment
  variables for specific SPIIR installations. This function can then be sourced
  from your shell startup script (e.g., ~/.bashrc or ~/.zshrc). (Note that we
  don't, at this time, support csh or other shells.)

  In this environment, you can run:
  $ load_spiir spiir_dev
  to load the spiir environment that was built with --name=spiir-dev; if not
  name is given, default to 'master'. You can also run:
  $ cd_src spiir_dev
  to switch to (a) load the spiir_dev environment, and (b) change working
  directory to the location that the SPIIR source was located. In principle,
  you may then make changes and run
  $ make && make install
  to recompile and reinstall, and similarly 'git commit' and friends should
  work as expected.

print-config
  Print out the command line arguments (including automatically set ones) and
  do nothing else.

The following options may be specified (see below for important note on the
defaults):

--help, -h
  Show this help screen and do nothing else.

--download-dir
  The directory to download sources to. Defaults to ./sources

--dep-prefix
  The installation prefix for dependencies. Defaults to ./dependencies

--tmpdir
  A temporary directory for running the compilation. If on a cluster with a
  network filesystem, ideally this will be a fast local disk. Defaults to a
  temporary directory under /tmp

--spiir-dir
  Directory containing the SPIIR sources and installed files. Note that the
  SPIIR git repository is cloned to [spiir-dir]/[name]/src and installed to
  [spiir-dir]/[name]/install. Defaults to ./spiir

--branch
  The SPIIR branch to check out. Default to 'master'.

--name
  The name to be used by 'load_spiir' to load the environment for this branch.
  Defaults to be the same as --branch.

--spiir-repository
  Git repository to clone from, defaults to
  'https://git.ligo.org/lscsoft/spiir.git'.

--num-jobs
  The number of compile jobs to run in parallel (i.e., passed to make as
  -j<...>). Defaults to 6

--use-special-python
  Rather than using the system Python, use the Python that's been installed in
  this place (e.g. an anaconda/miniconda install).

--singularity
  Rather than using the system environment, use a built singularity sandbox
  with all dependencies preinstalled.

--singularity-base
  Specify the path to the base singularity sandbox containing the environment
  to be used.

--py3
  Use the Python 3 sandbox.

--debug
  Add debug flags to the build process so that debuggers can step through code.

In some cases, where the script recognises your environment, some of the
defaults above will be overwritten. (Note that options specified on the command
line always take precedence.) The following environments are currently
recongised automatically:

GWDC on OzStar (machine hostname is '+(tooarrana|farnarkle)[12]', user is a member of the
oz996 group). ** CURRENTLY INACTIVE **
  --download-dir=/fred/oz996/pwc_build_script_tests/sources
  --dep-prefix=/fred/oz996/pwc_build_script_tests/dependencies
  --spiir-repository=/fred/oz996/pwc_build_script_tests/spiir.git
  --singularity
  --singularity-base=/fred/oz996/singularity/spiir-base-py2

  SPIIR on OzStar (machine hostname is '+(tooarrana|farnarkle)[12]', user is a member of the
oz016 group and not oz996).
  --download-dir=/fred/oz016/gwdc_spiir_pipeline_codebase/spiir_dependencies/sources
  --dep-prefix=/fred/oz016/gwdc_spiir_pipeline_codebase/spiir_dependencies/build_dependencies/
  --use-special-python=/fred/oz016/gwdc_spiir_pipeline_codebase/miniconda2
  --singularity
  --singularity-base=/fred/oz016/singularity/spiir-base-py2

CIT (machine hostname is '*.ligo.caltech.edu', user must be a member of the
spiir group and have access to /home/spiir).
  --singularity
  --singularity-base=/home/spiir/singularity/spiir-base-py2

The following environment variables are influential:

DONT_CLEANUP
  If DONT_CLEANUP=1, don't delete the build directory at the end of the script.

References:

[0] SPIIR - Summed Parallel Infinite Impulse Response pipeline for detecting
    compact binary coalesnce events. See for example Hooper et al Phys Rev D
    vol 86, 024012 (2012) or Chu, PhD Thesis (2017).
"
}

main() {
	local OPTS=$( getopt -l "help,download-dir:,dep-prefix:,tmpdir:,spiir-dir:,branch:,name:,spiir-repository:,num-jobs:,use-special-python,singularity:,singularity-base:,py3:,debug:" -o "h" -- "$@" )
	if [ $? -ne 0 ] ; then
		echo "Couldn't understand the options supplied"
		usage
		exit 1
	fi

	# Load up relevant default arguments
	local WHATTODO="help"
	local USE_SINGULARITY="no"
	local SINGULARITY_BASE=""
	local OPT_SINGULARITY_BASE=""
	local USE_PY3="no"
	local BIND_DIR=""
	local USE_DEBUG="no"
	local SOURCEDIR=$(pwd)/sources
	local DEPPREFIX=$(pwd)/dependencies
	local BUILDDIR="" # will create a temporary directory iff this remains blank
	local SPIIR_DIR=$(pwd)
	local BRANCH="master"
	local BRANCH_NAME=""
	#local SPIIR_REPO="https://git.ligo.org/lscsoft/gstlal.git" # another possible choice
	local SPIIR_REPO="https://git.ligo.org/lscsoft/spiir.git"
	local KNOWN_ENVIRONMENT="(none)"
	local PYTHON_OVERRIDE=
	NUM_JOBS=6 # not a local variable....

	# Load defaults for known environments
	if [[ $(hostname) == +(tooarrana|farnarkle)[12] ]] && id -nG | grep "\boz996\b" &>/dev/null && false ; then
		# This one is inactive -- everyone in oz996 is a member of oz016 and
		# it's a pain to otherwise deal with
		echo " ** I've detected you're a member of the ADACS group and we're running on OzSTAR"
		echo " Therefore I'll load special defaults."
		echo " Remember these can be overwritten by --whatever... on the command line"
		SOURCEDIR=/fred/oz996/pwc_build_script_tests/sources
		DEPPREFIX=/fred/oz996/pwc_build_script_tests/dependencies
		SPIIR_REPO=/fred/oz996/pwc_build_script_tests/spiir.git
		KNOWN_ENVIRONMENT="ADACS SPIIR dev group (oz996 on OzSTAR)"
		USE_SINGULARITY="yes"
		SINGULARITY_BASE="/fred/oz996/singularity/spiir-base-py2"
		BIND_DIR="--bind /home,/fred,/datasets"
	elif [[ $(hostname) == +(tooarrana|farnarkle)[12] ]] && id -nG | grep "\boz016\b" &>/dev/null ; then
		echo " ** I've detected you're a member of the UWA SPIIR group and we're running on OzSTAR"
		echo " Therefore I'll load special defaults."
		echo " Remember these can be overwritten by --whatever... on the command line"
		SOURCEDIR=/fred/oz016/gwdc_spiir_install/sources
		DEPPREFIX=/fred/oz016/gwdc_spiir_install/dependencies
		#PYTHON_OVERRIDE=/fred/oz016/gwdc_spiir_pipeline_codebase/miniconda2
		KNOWN_ENVIRONMENT="UWA SPIIR group (oz016 on OzSTAR)"
		USE_SINGULARITY="yes"
		SINGULARITY_BASE="/fred/oz016/singularity/spiir-base-py2"
		BIND_DIR="--bind /home,/fred,/datasets,/fred/oz016/gwdc_spiir_install/sources/lalsuite-extra/data/lalsimulation:/usr/spiir/share/lalsimulation"
	elif [[ "$(hostname -f)" == +(*"ligo.caltech.edu"|"node"*) ]]; then
		echo " ** I've detected you're on CIT"
		echo " Therefore I'll load special defaults."
		echo " Remember these can be overwritten by --whatever... on the command line"
		DEPPREFIX=/usr/spiir
		KNOWN_ENVIRONMENT="CIT"
		USE_SINGULARITY="yes"
		SINGULARITY_BASE="/home/spiir/singularity/spiir-base-py2"
		BIND_DIR="--bind /home"
	fi

	# Read in the arguments
	eval set -- "$OPTS"

	while true; do

		case $1 in
			--help|-h)
				WHATTODO=help
				break
				;;

			--download-dir)
				shift
				SOURCEDIR=$1
				;;

			--dep-prefix)
				shift
				DEPPREFIX=$1
				;;

			--tmpdir)
				shift
				BUILDDIR=$1
				;;

			--spiir-dir)
				shift
				SPIIR_DIR=$1
				;;

			--branch)
				shift
				BRANCH=$1
				;;

			--name)
				shift
				BRANCH_NAME=$1
				;;

			--spiir-repository)
				shift
				SPIIR_REPO=$1
				;;

			--num-jobs)
				shift
				NUM_JOBS=$1
				;;

			--use-special-python)
				shift
				PYTHON_OVERRIDE=$1
				;;

			--singularity)
				shift
				USE_SINGULARITY=$1
				;;

			--singularity-base)
				shift
				USE_SINGULARITY="yes"
				OPT_SINGULARITY_BASE=$1
				;;

			--py3)
				shift
				USE_SINGULARITY="yes"
				USE_PY3="yes"
				SINGULARITY_BASE="${SINGULARITY_BASE::-1}3"
				;;

			--debug)
				shift
				USE_DEBUG="yes"
				;;

			--)
				shift
				# next argument will be what to do
				WHATTODO=${1:-help}
				break # and now we can leave the loop
				;;

			*)
				echo "Unrecognised argument"
				exit 1;
				;;

		esac

		shift

	done

	if [ -z "$BRANCH_NAME" ] ; then
		# if not specified, branch_name should be the same as the name of the
		# branch
		# ... in retrospect, these were not especially well-named -- for this
		# reason the user interface just calls this the "name"
		BRANCH_NAME=$BRANCH
	fi

	if [ -n "$OPT_SINGULARITY_BASE" ] ; then
		SINGULARITY_BASE=$OPT_SINGULARITY_BASE	
	fi

	if [ "$WHATTODO" = "help" ] ; then
		usage
		exit 0
	elif [ "$WHATTODO" = "print-config" ] ; then
		echo "Configuration: task is $WHATTODO, loaded defaults from $KNOWN_ENVIRONMENT
--download-dir=$SOURCEDIR
--dep-prefix=$DEPPREFIX
--tmpdir=$BUILDDIR
--spiir-dir=$SPIIR_DIR
--branch=$BRANCH
--name=$BRANCH_NAME
--spiir-repository=$SPIIR_REPO
--use-special-python=$PYTHON_OVERRIDE
--singularity=$USE_SINGULARITY
--singularity-base=$SINGULARITY_BASE
--py3=$USE_PY3
--debug=$USE_DEBUG"
		exit 0
	fi

	if [ -z "$DEPPREFIX" ] ; then
		echo "Please specify the dependency prefix (where to install dependencies) using --dep-prefix"
		exit 1
	fi

	# Commented out these two echo()s because they cause confusion
	#echo "You asked to install dependencies to '$DEPPREFIX'"
	DEPPREFIX=$(realpath $DEPPREFIX)
	#echo "If it wasn't already, I changed this to fully-qualified path '$DEPPREFIX'"

	if [ "$WHATTODO" = "environment" ] ; then

		# Figure out which modules to load. For now we module purge and see if
		# git is still available; if it is we assume no modules need to be
		# loaded, otherwise we load all the OzStar modules
		# TODO: make this generic
		module_purge
		local MODULES=""
		local MODULES_PYTHON3=""
		# Similarly if there is a special Python, then we assume that the
		# Python related dependencies (numpy, scipy, swig, matplotlib) are
		# already installed and don't load those modules
		local PYTHON_RELATED_MODULES="python/2.7.14 numpy/1.14.1-python-2.7.14 scipy/1.0.0-python-2.7.14 swig/3.0.12-python-2.7.14 matplotlib/2.2.2-python-2.7.14"
		if [ -n "$PYTHON_OVERRIDE" ] ; then
			PYTHON_RELATED_MODULES=""
		fi

		if type module &> /dev/null && ! type git &> /dev/null ; then
			MODULES="module load git/2.18.0 gcc/6.4.0 gsl/2.5 openmpi/3.0.0 fftw/3.3.7 framel/8.30 metaio/8.4.0 hdf5/1.10.1-serial cuda/10.0.130 healpix/3.50 lapack/3.8.0 h5py/2.7.1-python-2.7.14-serial healpy/1.12.4-python-2.7.14 astropy/2.0.3-python-2.7.14 $PYTHON_RELATED_MODULES"
			# For unload, also need to remove some dependencies
			MODULES_UNLOAD="module unload git/2.18.0 gcc/6.4.0 gsl/2.5 openmpi/3.0.0 fftw/3.3.7 framel/8.30 metaio/8.4.0 hdf5/1.10.1-serial cuda/10.0.130 healpix/3.50 lapack/3.8.0 binutils/2.30 gcccore/6.4.0 szip/2.1.1 libpng/1.2.59 openblas/0.2.20 scalapack/2.0.2-openblas-0.2.20 cfitsio/3.450 pgplot/5.2.2 sqlite/3.21.0 $PYTHON_RELATED_MODULES"
			MODULES_PYTHON3="module load git/2.18.0 binutils/2.30 gcccore/6.4.0 gcc/6.4.0 gsl/2.5 openmpi/3.0.0 fftw/3.3.7 framel/8.30 openblas/0.2.20 metaio/8.4.0 hdf5/1.10.1-serial cfitsio/3.450 pgplot/5.2.2 healpix/3.50 python/3.7.4 swig/4.0.2-python-3.7.4"
			MODULE_SINGLARITY="module load apptainer || module load apptainer/latest"
			MODULE_UNLOAD_SINGLARITY="module unload apptainer || module unload apptainer/latest"
		else
			MODULES="echo n.b. Not loading any modules"
			MODULES_UNLOAD="echo No modules to unload"
			MODULES_PYTHON3="echo n.b. Not loading any modules"
			MODULE_SINGLARITY="echo n.b. Not loading any modules"
			MODULE_UNLOAD_SINGLARITY="echo No modules to unload"
		fi


		# I don't want to pollute the code below with a comment, but for
		# posterity: we need to manipulate the environment variables to (a)
		# add the paths to the dependencies *if they're not already there*, (b)
		# remove the old SPIIR paths (if any), and (c) add the new SPIIR paths.
		# This turns out to be more difficult than one might imagine in plain
		# bash, simply because the environment variables are ':'-separated
		# strings, and bash's string/array manipulation could be better.
		# Thus we use a short Perl script that splits the string, filters out
		# the dependency and old SPIIR paths, and then appends the dependency
		# and new SPIIR paths, and puts the string back together. This is not
		# ideal (we remove and then add the dependency path) but it saves us
		# having to deal with a special case. The script takes four (command
		# line) arguments: the current value of the variable (e.g. $PATH), the
		# dependency path, the *old* SPIIR path, and the *new* SPIIR path.
		# (Passing these as arguments means we can lean on the shell for
		# quoting them correctly.)
		# The script is (best to read from bottom to top):
		# export PATH=$(
		# perl -e '@r{@ARGV[1,2]}=undef;    # paths to delete
		#          CORE::say join ":",      # join and print
		#          (@ARGV[3,2],             # prepend the new paths
		#          grep {!exists($r{$_})}   # filter out old paths
		#          split /:/, $ARGV[0])'    # split into an array
		#          "\$PATH" "$DEPPREFIX/bin" "\$OSP/bin" "\$SPIIR_PATH/bin")
		# Irritatingly there are three version of this. PATH, GST_PLUGIN_PATH,
		# PKG_CONFIG_PATH need to include the paths from the dependencies and
		# the SPIIR install and use the script above. LAL_DATA_PATH doesn't
		# have analogues in the SPIIR install (just dependencies) so the '%r'
		# hash has only one element. LD_LIBRARY_PATH and PYTHONPATH need 2*2 =
		# 4 cases to cover (SPIIR, dependencies) x (lib, lib64) and so there
		# are more elemets in %r and prepended.
		#
		# As a further wrinkle, if PYTHON_OVERRIDE is specified, then we need
		# to add an extra thing each to both the PATH and PYTHONPATH.
		#
		# There is also the question of which environment variables to set. The
		# minimum is probably:
		#  PYTHONPATH
		#  PATH
		#  LD_LIBRARY_PATH
		#  GST_PLUGIN_PATH
		# but we also set:
		#  LAL_DATA_PATH
		#  PKG_CONFIG_PATH
		# because some things may rely on them, and we want to be able to
		# compile. Finally we set SPIIR_PATH to the base path for this SPIIR
		# branch, both so the script knows what to remove, and also because
		# some other scripts may at some future point find them useful.
		#
		# The unload_spiir function has the same sort of issues, but is a bit
		# easier, because we can assume that the SPIIR paths are in there and
		# just unconditionally remove them.

		local PATH_THING
		local PYTHONPATH_THING
		if [ -z "$PYTHON_OVERRIDE" ] ; then
			# normal
			PATH_THING="export PATH=\$(perl -e '@r{@ARGV[1,2]}=undef; CORE::say join \":\", (@ARGV[3,1], grep {!exists(\$r{\$_})} split /:/, \$ARGV[0])' \"\$PATH\" \"$DEPPREFIX/bin\" \"\$OSP/install/bin\" \"\$SPIIR_PATH/install/bin\")"
			PYTHONPATH_THING="export PYTHONPATH=\$(perl -e '@r{@ARGV[1..4]}=undef; CORE::say join \":\", (@ARGV[5,6,1,2], grep {!exists(\$r{\$_})} split /:/, \$ARGV[0])' \"\$PYTHONPATH\" \"$DEPPREFIX/lib/python2.7/site-packages\" \"$DEPPREFIX/lib64/python2.7/site-packages\" \"\$OSP/install/lib/python2.7/site-packages\" \"\$OSP/install/lib64/python2.7/site-packages\" \"\$SPIIR_PATH/install/lib/python2.7/site-packages\" \"\$SPIIR_PATH/install/lib64/python2.7/site-packages\")"

			UNLOAD_PATH_THING="export PATH=\$(perl -e '@r{@ARGV[1,2]}=undef; CORE::say join \":\", (grep {!exists(\$r{\$_})} split /:/, \$ARGV[0])' \"\$PATH\" \"$DEPPREFIX/bin\" \"\$SPIIR_PATH/install/bin\")"
			UNLOAD_PYTHONPATH_THING="export PYTHONPATH=\$(perl -e '@r{@ARGV[1..4]}=undef; CORE::say join \":\", (grep {!exists(\$r{\$_})} split /:/, \$ARGV[0])' \"\$PYTHONPATH\" \"$DEPPREFIX/lib/python2.7/site-packages\" \"$DEPPREFIX/lib64/python2.7/site-packages\" \"\$SPIIR_PATH/install/lib/python2.7/site-packages\" \"\$SPIIR_PATH/install/lib64/python2.7/site-packages\")"
		else
			# special Python stuff
			PATH_THING="export PATH=\$(perl -e '@r{@ARGV[1..3]}=undef; CORE::say join \":\", (@ARGV[4,2,1], grep {!exists(\$r{\$_})} split /:/, \$ARGV[0])' \"\$PATH\" \"$DEPPREFIX/bin\" \"$PYTHON_OVERRIDE/bin\" \"\$OSP/install/bin\" \"\$SPIIR_PATH/install/bin\")"
			PYTHONPATH_THING="export PYTHONPATH=\$(perl -e '@r{@ARGV[1..5]}=undef; CORE::say join \":\", (@ARGV[6,7,1,2,3], grep {!exists(\$r{\$_})} split /:/, \$ARGV[0])' \"\$PYTHONPATH\" \"$DEPPREFIX/lib/python2.7/site-packages\" \"$DEPPREFIX/lib64/python2.7/site-packages\" \"$PYTHON_OVERRIDE/lib/python2.7/site-packages\" \"\$OSP/install/lib/python2.7/site-packages\" \"\$OSP/install/lib64/python2.7/site-packages\" \"\$SPIIR_PATH/install/lib/python2.7/site-packages\" \"\$SPIIR_PATH/install/lib64/python2.7/site-packages\")"

			UNLOAD_PATH_THING="export PATH=\$(perl -e '@r{@ARGV[1..3]}=undef; CORE::say join \":\", (grep {!exists(\$r{\$_})} split /:/, \$ARGV[0])' \"\$PATH\" \"$DEPPREFIX/bin\" \"$PYTHON_OVERRIDE/bin\" \"\$SPIIR_PATH/install/bin\")"
			UNLOAD_PYTHONPATH_THING="export PYTHONPATH=\$(perl -e '@r{@ARGV[1..5]}=undef; CORE::say join \":\", (grep {!exists(\$r{\$_})} split /:/, \$ARGV[0])' \"\$PYTHONPATH\" \"$DEPPREFIX/lib/python2.7/site-packages\" \"$DEPPREFIX/lib64/python2.7/site-packages\" \"$PYTHON_OVERRIDE/lib/python2.7/site-packages\" \"\$SPIIR_PATH/install/lib/python2.7/site-packages\" \"\$SPIIR_PATH/install/lib64/python2.7/site-packages\")"
		fi

		cat > $SPIIR_DIR/bash_helper_functions.sh <<END
load_spiir() {
	local BRANCH=\${1:-master}	
	if [ -e $SPIIR_DIR/\$BRANCH ] ; then
		if [[ "\${USE_SINGULARITY:-$USE_SINGULARITY}" == "yes" ]]; then
			$MODULE_SINGLARITY
			local SPIIR_PATH=$SPIIR_DIR/\$BRANCH

			if [[ -n \${CD_SPIIR:-} ]]; then
				cd \$SPIIR_PATH/source/gstlal-spiir
			fi
			
			if [[ -n \${RUN_SPIIR:-} ]]; then
				local SHELL_OR_EXEC=exec
			else
				local SHELL_OR_EXEC=shell
			fi

			if [[ -n \${PY3_SPIIR:-} ]]; then
				GST_VER="gstreamer-1.0"
				PY_VER="python3.10"
				local SINGULARITY_BASE=\${SINGULARITY_BASE:-"${SINGULARITY_BASE::-1}3"}
			else
				GST_VER="gstreamer-0.10"
				PY_VER="python2.7"
				local SINGULARITY_BASE=\${SINGULARITY_BASE:-$SINGULARITY_BASE}
			fi

			if [[ -n \${SLURM_CPUS_PER_TASK} ]]; then
				# Using SLURM, some libraries don't know the allocated CPUs and instead try use all.
				env_args="--env OMP_NUM_THREADS=\$SLURM_CPUS_PER_TASK \\
					--env OPENBLAS_NUM_THREADS=\$SLURM_CPUS_PER_TASK \\
					--env MKL_NUM_THREADS=\$SLURM_CPUS_PER_TASK \\
					--env VECLIB_MAXIMUM_THREADS=\$SLURM_CPUS_PER_TASK \\
					--env NUMEXPR_NUM_THREADS=\$SLURM_CPUS_PER_TASK \\
					\$env_args"
			fi

			singularity \$SHELL_OR_EXEC \\
				--cleanenv \\
				--nv \\
				--bind \$SPIIR_PATH/source:/repo $BIND_DIR \\
				--env GST_DEBUG="\$GST_DEBUG" \\
				--env GST_REGISTRY_UPDATE="no" \\
				--env GST_REGISTRY="\$SPIIR_PATH/install/gst-registry.bin" \\
				--env X509_USER_PROXY="\$X509_USER_PROXY" \\
				--env X509_USER_KEY="\$X509_USER_KEY" \\
				--env X509_USER_CERT="\$X509_USER_CERT" \\
				--env KRB5_KTNAME="\$KRB5_KTNAME" \\
				--env PATH="\$SPIIR_PATH/install/bin:\\\$PATH" \\
				--env PYTHONPATH="\$SPIIR_PATH/install/lib/\${PY_VER}/site-packages:\$SPIIR_PATH/install/lib64/\${PY_VER}/site-packages:/usr/spiir/lib/\${PY_VER}/site-packages" \\
				--env PKG_CONFIG_PATH="\$SPIIR_PATH/install/lib/pkgconfig:\\\$PKG_CONFIG_PATH" \\
				--env GST_PLUGIN_PATH="\$SPIIR_PATH/install/lib/\${GST_VER}:\\\$GST_PLUGIN_PATH" \\
				--env LD_LIBRARY_PATH="\$SPIIR_PATH/install/lib:\$SPIIR_PATH/install/lib64:\\\$LD_LIBRARY_PATH" \\
				\${env_args:-} \\
				\$SINGULARITY_BASE \\
				\$CMD
			$MODULE_UNLOAD_SINGLARITY
			unset SPIIR_PATH
		else
			$MODULES
			local OSP=\${SPIIR_PATH:-*}
			export SPIIR_PATH=$SPIIR_DIR/\$BRANCH
			$PATH_THING
			export PKG_CONFIG_PATH=\$(perl -e '@r{@ARGV[1,2]}=undef; CORE::say join ":", (@ARGV[3,1], grep {!exists(\$r{\$_})} split /:/, \$ARGV[0])' "\$PKG_CONFIG_PATH" "$DEPPREFIX/lib/pkgconfig" "\$OSP/install/lib/pkgconfig" "\$SPIIR_PATH/install/lib/pkgconfig")
			export GST_PLUGIN_PATH=\$(perl -e '@r{@ARGV[1,2]}=undef; CORE::say join ":", (@ARGV[3,1], grep {!exists(\$r{\$_})} split /:/, \$ARGV[0])' "\$GST_PLUGIN_PATH" "$DEPPREFIX/lib/gstreamer-0.10" "\$OSP/install/lib/gstreamer-0.10" "\$SPIIR_PATH/install/lib/gstreamer-0.10")
			$PYTHONPATH_THING
			export LD_LIBRARY_PATH=\$(perl -e '@r{@ARGV[1..4]}=undef; CORE::say join ":", (@ARGV[5,6,1,2], grep {!exists(\$r{\$_})} split /:/, \$ARGV[0])' "\$LD_LIBRARY_PATH" "$DEPPREFIX/lib" "$DEPPREFIX/lib64" "\$OSP/install/lib" "\$OSP/install/lib64" "\$SPIIR_PATH/install/lib" "\$SPIIR_PATH/install/lib64")
			export LAL_DATA_PATH=\$(perl -e '@r{@ARGV[1]}=undef; CORE::say join ":", (@ARGV[1], grep {!exists(\$r{\$_})} split /:/, \$ARGV[0])' "\$LAL_DATA_PATH" "$DEPPREFIX/share/lalsimulation")
		fi
	else
		echo "Can't load environment for \$BRANCH because '$SPIIR_DIR/\$BRANCH' doesn't exist - you may need to run build-spiir.sh with an appropriate --name parameter."
	fi
}

run_spiir() {
        local OPTIND opt ENVS BRANCH CMD
        while getopts "e:h" opt; do
                case \${opt} in
                        e)
				ENVS+=("\$OPTARG")
				;;
                        h)
				printf "Run with optional env vars set:   run_spiir -e <name>=<value> -e <name>=<value> <build name> <command to run>\n"
				return 1
				;;
                        *)
				return 1 # illegal option
                esac
        done

        shift \$((OPTIND-1))
        BRANCH=\$1
        CMD=\${@:2}

        if [[ -z \${BRANCH} ]]; then
                echo "Must supply build name."
                return 1
        fi

        if [[ -z \${CMD} ]]; then
                echo "Must supply command to run."
                return 1
        fi

        local env_args=""
        for env in "\${ENVS[@]}"; do
                env_args="\$env_args --env \$env"
        done

	local RUN_SPIIR=1
	load_spiir "\$@"
}

unload_spiir() {
	if [ -z "\${SPIIR_PATH:-}" ] ; then
		echo SPIIR not loaded so doing nothing
	else
		purge_spiir_modules
		purge_spiir_environment
	fi
	unset SPIIR_PATH
}

purge_spiir_modules() {
	if [ -z "\${SPIIR_PATH:-}" ] ; then
		echo SPIIR not loaded so doing nothing
	else
		$MODULES_UNLOAD
	fi
}

purge_spiir_environment() {
	if [ -z "\${SPIIR_PATH:-}" ] ; then
		echo SPIIR not loaded so doing nothing
	else
		$UNLOAD_PATH_THING
		export PKG_CONFIG_PATH=\$(perl -e '@r{@ARGV[1,2]}=undef; CORE::say join ":", (grep {!exists(\$r{\$_})} split /:/, \$ARGV[0])' "\$PKG_CONFIG_PATH" "$DEPPREFIX/lib/pkgconfig" "\$SPIIR_PATH/install/lib/pkgconfig")
		export GST_PLUGIN_PATH=\$(perl -e '@r{@ARGV[1,2]}=undef; CORE::say join ":", (grep {!exists(\$r{\$_})} split /:/, \$ARGV[0])' "\$GST_PLUGIN_PATH" "$DEPPREFIX/lib/gstreamer-0.10" "\$SPIIR_PATH/install/lib/gstreamer-0.10")
		$UNLOAD_PYTHONPATH_THING
		export LD_LIBRARY_PATH=\$(perl -e '@r{@ARGV[1..4]}=undef; CORE::say join ":", (grep {!exists(\$r{\$_})} split /:/, \$ARGV[0])' "\$LD_LIBRARY_PATH" "$DEPPREFIX/lib" "$DEPPREFIX/lib64" "\$SPIIR_PATH/install/lib" "\$SPIIR_PATH/install/lib64")
		export LAL_DATA_PATH=\$(perl -e '@r{@ARGV[1]}=undef; CORE::say join ":", (grep {!exists(\$r{\$_})} split /:/, \$ARGV[0])' "\$LAL_DATA_PATH" "$DEPPREFIX/share/lalsimulation")
	fi
}

cd_spiir() {
	local CD_SPIIR=1
	load_spiir "\$@"
}

load_spiir_py3() {
	local PY3_SPIIR=1
	load_spiir "\$@"
}

cd_spiir_py3() {
	local PY3_SPIIR=1
	cd_spiir "\$@"
}

run_spiir_py3() {
	local PY3_SPIIR=1
	run_spiir "\$@"
}

ls_spiir() {
	for d in $SPIIR_DIR/* ; do
		[ -e "\$d/source/.git" ] || continue
		BRANCH=\$(git -C \$d/source rev-parse --symbolic-full-name --abbrev-ref HEAD 2>/dev/null)
		printf "% -40s  %s\n" \$(basename \$d) "(branch '\$BRANCH')"
	done
}

spiir_python2to3() {
	if [ -z "\${SPIIR_PATH:-}" ] ; then
		echo SPIIR not loaded so doing nothing
	else
		export PYTHONPATH=$DEPPREFIX/python3/lib/python3.7/site-packages/:\$PYTHONPATH
		export PATH=$DEPPREFIX/python3/bin:\$PATH
		module purge
		$MODULES_PYTHON3
	fi
}

spiir_python3to2() {
	if [ -z "\${SPIIR_PATH:-}" ] ; then
		echo SPIIR not loaded so doing nothing
	else
		export PYTHONPATH=\$(perl -e 'CORE::say join ":", (grep {\$_ ne \$ARGV[1]} split /:/, \$ARGV[0])' "\$PYTHONPATH" "$DEPPREFIX/python3/lib/python3.7/site-packages/")
		export PATH=\$(perl -e 'CORE::say join ":", (grep {\$_ ne \$ARGV[1]} split /:/, \$ARGV[0])' "\$PYTHONPATH" "$DEPPREFIX/python3/bin")
		module purge
		$MODULES
	fi
}

help_spiir() {
	echo "These SPIIR-specific commands are supplied by the build-spiir script:

help_spiir        -- this help screen
ls_spiir          -- list all SPIIR installs
load_spiir [name] -- set up the environment for the SPIIR install called "name"
run_spiir [-e ENV=VALUE] [name] [command to run] -- run command using singularity environment
unload_spiir      -- unload spiir related modules, remove SPIIR from env vars
purge_spiir_modules     -- unload all spiir related modules
purge_spiir_environment -- remove SPIIR from env vars
cd_spiir [name]   -- set up the environment and then change to the source dir
[load/run/cd]_spiir_py3 -- Use function with python 3 singularity sandbox.
spiir_python2to3  -- add Python 3 libraries to path
spiir_python3to2  -- remove Python 3 libraries from path

By default commands taking a 'name' argument default to 'master' if not
specified."
}
END

		cat <<END
I've written some useful helper functions here:
 $SPIIR_DIR/bash_helper_functions.sh
You can add this line to your shell startup script (typically ~/.bashrc,
~/.zshrc or similar) to automatically load them on startup:

. $SPIIR_DIR/bash_helper_functions.sh

(note the leading '.') 

You must restart the shell to enable use of the functions.
Try running help_spiir for more info.
END
		exit 0
	fi

	# Check the remaining directories
	if [ -z "$SOURCEDIR" ] ; then
		echo "Please specify sourcedir"
		exit 1
	fi
	mkdir -p "$SOURCEDIR"

	# We used to download everything. However now we only download stuff if
	# we're explicitly asked to - because it's not something that needs to
	# happen for people who already have the dependencies set up
	if [ "$WHATTODO" = "download" ] ; then
		download_stuff $SOURCEDIR
		exit 0
	fi

	# If we're in a 'load_spiir' environment, get out of it
	unload_spiir_environment $DEPPREFIX $SPIIR_DIR

	if [[ "$USE_SINGULARITY" == "no" ]]; then
		# Load 'system provided' dependencies (mostly stuff from 'module load')
		module_purge
		for thing in git gcc gsl openmpi fftw python2 numpy scipy swig-python framel metaio hdf5 matplotlib-python cuda healpix lapack libxml2 ; do
			load_preinstalled $thing '' $PYTHON_OVERRIDE
		done
	fi

	prepare_environment $DEPPREFIX # pitfall - want to call this after loading the modules
	echo "Prefix is: $PREFIX"

	if [ "$WHATTODO" = "dependencies" ] ; then
		# Prepare the build directory -- note that now this is used only to
		# build the dependencies, not anything else
		if [ -e "$BUILDDIR" ] ; then
			echo "I'm too cowardly to build using a temporary directory that already exists (because I delete it at the end) - please specify one that doesn't"
			exit 1
		fi

		if [ -z "$BUILDDIR" ] ; then
			BUILDDIR=$(mktemp -d)
			echo "I'm going to use this temporary directory for builds -> $BUILDDIR"
		fi

		mkdir -p "$BUILDDIR"

		# Do the build
		build_dependencies $SOURCEDIR $BUILDDIR

		# And clean up
		if [ $DONT_CLEANUP == "1" ] ; then
			echo "Skipping cleanup, build files are in $BUILDDIR"
		else
			echo "Cleaning up now..."
			rm -rfv $BUILDDIR
		fi
	elif [ "$WHATTODO" = "build-spiir" ] ; then
		# We need to clone spiir, check out the correct branch, and compile
		# The correct branch is given by 'BRANCH', but the place we compile and
		# install to is given by 'SPIIR_DIR' and 'BRANCH_NAME':
		#  clone to $SPIIR_DIR/$BRANCH_NAME/source
		#  install to $SPIIR_DIR/$BRANCH_NAME/install
		# If the clone target already exists, then we assume it's been cloned
		# etc correctly and just build

		if [ -e "$SPIIR_DIR/$BRANCH_NAME/source" ] ; then
			echo "It looks like the source has already been cloned ($SPIIR_DIR/$BRANCH_NAME/source) so I'll go straight to compile it"
		else
			load_git
			git_clone $SPIIR_REPO $BRANCH "$SPIIR_DIR/$BRANCH_NAME/source"
		fi
		if [[ "$USE_SINGULARITY" == "yes" ]]; then
			if [[ "$USE_PY3" == "yes" ]]; then
				build_spiir_py3_with_singularity
			else
				build_spiir_with_singularity
			fi
		else
			build_spiir_without_singularity $SPIIR_DIR $DEPPREFIX $BRANCH $BRANCH_NAME
		fi
	else
		echo "Unrecognised thing to do - $WHATTODO"
		usage
		exit 0
	fi

}

download_stuff() {
	local SRC=$1

	load_git # needed for the git_clone calls
	load_preinstalled git_lfs # needed for lalsuite-extra
	pushd $SRC
	git_clone https://git.ligo.org/lscsoft/lalsuite.git
	git_clone https://git.ligo.org/lscsoft/glue.git tags/glue-release-1.59.2
	#git_clone https://git.ligo.org/lscsoft/gstlal.git spiir # Moved to build-spiir
	git_clone https://git.ligo.org/lscsoft/lalsuite-extra
	# This is a bit of a special case -- for lalsuite-extra we need to do some weird LFS stuff
	pushd lalsuite-extra
	git lfs checkout "data/lalsimulation/*.hdf5"
	popd
	git_clone_single_branch https://gitlab.freedesktop.org/gstreamer/gstreamer.git 0.10
	git_clone_single_branch https://gitlab.freedesktop.org/gstreamer/gst-plugins-base.git 0.10
	git_clone_single_branch https://gitlab.freedesktop.org/gstreamer/gst-plugins-good.git 0.10
	git_clone_single_branch https://gitlab.freedesktop.org/gstreamer/gst-python.git 0.10
	wget -nc https://ftp.gnu.org/pub/gnu/gettext/gettext-0.20.1.tar.gz
	wget -nc http://mirror.pnl.gov/macports/distfiles/ldas-tools-al/ldas-tools-al-2.5.7.tar.gz
	wget -nc http://mirror.pnl.gov/macports/distfiles/ldas-tools-framecpp/ldas-tools-framecpp-2.5.8.tar.gz
	wget -nc https://ftp.gnome.org/pub/GNOME/sources/gobject-introspection/1.63/gobject-introspection-1.63.1.tar.xz
	wget -nc https://www.cairographics.org/releases/pixman-0.38.4.tar.gz
	wget -nc https://www.cairographics.org/releases/cairo-1.16.0.tar.xz
	wget -nc https://ftp.acc.umu.se/pub/GNOME/sources/pygobject/2.28/pygobject-2.28.7.tar.xz
	wget -nc https://ftp.gnome.org/pub/GNOME/sources/pygtk/2.24/pygtk-2.24.0.tar.bz2
	wget -nc https://ftp.gnome.org/pub/gnome/sources/glib/2.62/glib-2.62.3.tar.xz
	wget -nc https://mirrors.edge.kernel.org/pub/linux/utils/util-linux/v2.34/util-linux-2.34.tar.xz

	# If GSL is old, we'll need to download and build a new version
	load_preinstalled gcc
	load_preinstalled gsl
	if ! pkg-config --atleast-version=2.5 gsl ; then
		wget -nc ftp://ftp.gnu.org/gnu/gsl/gsl-2.6.tar.gz
	fi
	popd
}

unload_spiir_environment() {
	local DEPPREFIX=$1
	local SPIIR_DIR=$2
	# Normally we are agnostic to weird things people want to put in their
	# PATH, PYTHONPATH etc -- this means that the script is durable and will
	# run in a range of environments. However, the one exception is other SPIIR
	# installs -- they will mess things up.
	# For this reason, if we detect we're in a SPIIR environment (i.e., if
	# someone has run load_spiir and thus set the SPIIR_PATH variable), then we
	# remove the SPIIR-related dependencies.

	# Removing these things is a bit complicated because in principle anything
	# could have been added to the path. Perhaps in the future we will store
	# special environment variables (analogous to $SPIIR_PATH) that tell us
	# exactly what to remove, but for now we just assume the complication is
	# happening with the same DEPPREFIX and SPIIR_DIR
	if [ -z "${SPIIR_PATH:-}" ] ; then
		# No "SPIIR_PATH" set? In that case we don't need to do anything
		return
	fi

	# Otherwise we need to clear PATH, PKG_CONFIG_PATH, PYTHONPATH,
	# LD_LIBRARY_PATH, GST_PLUGIN_PATH, LAL_DATA_PATH
	export PATH=$(perl -e '@r{@ARGV[1..$#ARGV]}=undef; print join ":", grep {!exists($r{$_})} split /:/, $ARGV[0]' "$PATH" "$DEPPREFIX/bin" "$SPIIR_PATH/install/bin")
	export PKG_CONFIG_PATH=$(perl -e '@r{@ARGV[1..$#ARGV]}=undef; print join ":", grep {!exists($r{$_})} split /:/, $ARGV[0]' "$PKG_CONFIG_PATH" "$DEPPREFIX/lib/pkgconfig" "$SPIIR_PATH/install/lib/pkgconfig")
	export PYTHONPATH=$(perl -e '@r{@ARGV[1..$#ARGV]}=undef; print join ":", grep {!exists($r{$_})} split /:/, $ARGV[0]' "$PYTHONPATH" "$DEPPREFIX/lib/python2.7/site-packages" "$DEPPREFIX/lib64/python2.7/site-packages", "$SPIIR_PATH/install/lib/python2.7/site-packages" "$SPIIR_PATH/install/lib64/python2.7/site-packages")
	export LD_LIBRARY_PATH=$(perl -e '@r{@ARGV[1..$#ARGV]}=undef; print join ":", grep {!exists($r{$_})} split /:/, $ARGV[0]' "$LD_LIBRARY_PATH" "$DEPPREFIX/lib" "$DEPPREFIX/lib64" "$SPIIR_PATH/install/lib" "$SPIIR_PATH/install/lib64")
	export GST_PLUGIN_PATH=$(perl -e '@r{@ARGV[1..$#ARGV]}=undef; print join ":", grep {!exists($r{$_})} split /:/, $ARGV[0]' "$GST_PLUGIN_PATH" "$DEPPREFIX/lib/gstreamer-0.10")
	export LAL_DATA_PATH=$(perl -e '@r{@ARGV[1..$#ARGV]}=undef; print join ":", grep {!exists($r{$_})} split /:/, $ARGV[0]' "$LAL_DATA_PATH" "$DEPPREFIX/share/lalsimulation")
}

prepare_environment() {
	local DEPPREFIX=$1
	# This routine sets up the environment. Note that it *DOES NOT* export the
	# variables (because some things require crazy environment variables), just
	# puts relevant things in suggestively-named shell variables

	# Save these in case they're needed
	SYSTEM_PYTHONPATH=${PYTHONPATH:-}
	SYSTEM_PATH=${PATH:-}
	SYSTEM_PKG_CONFIG_PATH=${PKG_CONFIG_PATH:-}
	SYSTEM_LD_LIBRARY_PATH=${LD_LIBRARY_PATH:-}

	PREFIX=$DEPPREFIX
	PREFIX_DEPENDENCIES=$DEPPREFIX
	PYTHONPATH=$PREFIX/lib/python2.7/site-packages:$PREFIX/lib64/python2.7/site-packages:${PYTHONPATH-}
	BINPATH=$PREFIX/bin:$PATH
	PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig/:$PREFIX/lib64/pkgconfig:${PKG_CONFIG_PATH:-}
	LD_LIBRARY_PATH=$PREFIX/lib:$PREFIX/lib64:${LD_LIBRARY_PATH:-}

}

build_dependencies() {
	local SOURCES=$1
	local BUILDDIR=$2
	# This massive subroutine builds all the dependencies. Where possible we
	# like to use some helper functions, but a few things are crazy

	# GSL -- on OzStar/most systems, we already have modern GSL installed.
	# However CIT has an ancient GSL, so for that we build a new one
	if ! pkg-config --atleast-version=2.5 gsl ; then
		tar_configure_make_makeinstall $SOURCES/gsl-2.6.tar.gz $BUILDDIR/buildgsl
	fi

	## GNU gettext
	# Why do we need this? Beats me - there's a program called 'autopoint'
	# that's needed to run autogen for gstreamer 0.10. It doesn't seem to be on
	# OzSTAR, so we compile it now.
	if type autopoint &> /dev/null ; then
		echo "Skipping gettext as you already seem to have autopoint"
	else
		tar_configure_make_makeinstall $SOURCES/gettext-0.20.1.tar.gz $BUILDDIR/buildgettext
	fi

	## Gstreamer
	# For gstreamer-0.10, we need to compile and build two things - 'gstreamer',
	# 'gst-plugins-base'
	for GST in gstreamer gst-plugins-base ; do
		if [ "$GST" == "gstreamer" ] ; then
			apply_patch $SOURCES/$GST manoj_00_gstreamer.patch
		fi
		pushd $SOURCES/$GST
		NOCONFIGURE=1 PATH=$BINPATH ./autogen.sh
		configure_make_makeinstall $(pwd) $BUILDDIR/buildgst_$GST
		popd
	done

	pushd $SOURCES/gst-plugins-good
	NOCONFIGURE=1 PATH=$BINPATH ./autogen.sh
	# The build dies on OzStar for some reason in the v4l2 plugins. Since we
	# don't need them, we just don't try to build them
	configure_make_makeinstall $(pwd) $BUILDDIR/buildgst_gst-plugins-good "" --disable-gst_v4l2
	popd

	## ldas-tools-al and ldas-tools-framecpp
	# framecpp is needed for gstlal-ugly, and ldas-tools-al is a dependency for
	# framecpp so we install that first. I couldn't find legit distributions
	# for ldas-tools stuff, but there are source tarballs in Macports that seem
	# Note: these are preinstalled on CIT but maybe not the right versions?
	tar_configure_make_makeinstall $SOURCES/ldas-tools-al-2.5.7.tar.gz $BUILDDIR/build_ldasal
	build_framecpp $SOURCES/ldas-tools-framecpp-2.5.8.tar.gz $BUILDDIR/build_ldasframecpp

	if pkg-config --exists cairo gobject-introspection-1.0 ; then
		echo "Skipping libmount, glib, gobject-introspection, pixman, cairo because you already seem to have gobject-introspection and cairo"
	else
		# Need libmount from util-linux. We try to avoid building everything else
		# because it's a total pain
		tar_configure_make_makeinstall $SOURCES/util-linux-2.34.tar.xz $BUILDDIR/build_utillin "" --disable-use-tty-group --disable-all-programs --enable-libmount --enable-libblkid

		# glib, gobject-introspection
		# Need for Pygobject
		tar_meson_compile $SOURCES/glib-2.62.3.tar.xz $BUILDDIR/build_glib
		tar_meson_compile $SOURCES/gobject-introspection-1.63.1.tar.xz $BUILDDIR/build_gobj-ispect

		# pixman and cairo - needed for pygtk
		tar_configure_make_makeinstall $SOURCES/pixman-0.38.4.tar.gz $BUILDDIR/build_pixman
		tar_configure_make_makeinstall $SOURCES/cairo-1.16.0.tar.xz $BUILDDIR/build_cairo
	fi

	# pygobject - needed for pygtk
	tar_configure_make_makeinstall $SOURCES/pygobject-2.28.7.tar.xz $BUILDDIR/build_pygobject

	# pygtk
	tar_configure_make_makeinstall $SOURCES/pygtk-2.24.0.tar.bz2 $BUILDDIR/build_pygtk

	# pygst
	# gst-python needs -Wno-error
	# needs to be done after pygtk for dependency reasons
	for GST in gst-python ; do
		pushd $SOURCES/$GST
		NOCONFIGURE=1 PATH=$BINPATH ./autogen.sh
		configure_make_makeinstall $(pwd) $BUILDDIR/buildgst_$GST -Wno-error
		popd
	done

	## LALSUITE
	# Apply the patches to lalsuite.
	# The patches themselves are stored at the bottom of this file.
	# I had to do some crazy edits to them to make them work -- for the two
	# lalsuite patches, they needed the source file locations to be renamed
	# from 'src' to 'lib'
	if type lalapps_version &> /dev/null; then
		echo "Skipping building lalsuite as you already seem to have it"
	else
		if [ -e $SOURCES/lalsuite/lalinspiral/LALInspiralIIR.c ] ; then
			echo "have tested -> $SOURCES/lalsuite/lalinspiral/LALInspiralIIR.c"
			apply_patch $SOURCES/lalsuite lalsuite_0000_cleanup.patch
		fi
		apply_patch $SOURCES/lalsuite lalsuite_0001_variable_epsilon.patch
		# The below patch is no longer needed - now merged in
		#apply_patch $SOURCES/lalsuite lalsuite_0002_libxml_header.patch

		pushd $SOURCES/lalsuite
		./00boot
		if [ -e "/apps/skylake/software/compiler/gcc/6.4.0/healpix/3.50/lib/chealpix.pc" ] ; then
			# Special hack for OzStar, where the chealpix module seems to have
			# some problems...
			PKG_CONFIG_PATH=/apps/skylake/software/compiler/gcc/6.4.0/healpix/3.50/lib/:$PKG_CONFIG_PATH
		fi
		configure_make_makeinstall $(pwd) $BUILDDIR/buildlalsuite
		popd

		pushd $SOURCES/lalsuite-extra
		./00boot
		configure_make_makeinstall $(pwd) $BUILDDIR/buildlalextra
		popd
	fi

	## GLUE
	apply_patch $SOURCES/glue glue_0000_zipsafe.patch

	pushd $SOURCES/glue
	PYTHONPATH=$PYTHONPATH python setup.py install --prefix=$PREFIX
	popd

	## Cython
	# On OzStar this is not a problem, it comes when we module load python/2.7.
	# However on CIT we need to install it ourselves -- thankfully PIP can do
	# this
	if type cython &> /dev/null ; then
		echo "Skipping Cython as you already seem to have it"
	else
		pip install Cython --prefix=$PREFIX
	fi

	# some more pip installs for various pipeline dependencies
	pip install ligo-segments --prefix=$PREFIX
	pip install shapely --prefix=$PREFIX

	pip install yapf --prefix=$PREFIX
	pip install clang-format --prefix=$PREFIX
	pip install pre-commit --prefix=$PREFIX

	python3_dependencies "$SOURCES" "$BUILDDIR"
}

python3_dependencies() {
	### PYTHON 3
	# SPIIR is a mostly-Python-2 code. However, progressively more and more
	# bits of it are being rewritten in Python 3, and we need to provide some
	# support for it.
	# There are two axes of this: first, there are some SWIG bindings that
	# depend on the Python version. For this reason, we compile a separate copy
	# of 'lalsuite' in a "Python 3" environment. Second, there are various
	# libraries we want. These we handle with pip.
	# The Python3 libraries themselves are stored in $PREFIX/python3. These
	# will eventually be PREPENDED to PATH, PYTHONPATH etc so the Python 3
	# libraries will override the Python 2 ones.
	# (Note that there's also a python3_stuff -- that is for 'meson' and
	# 'ninja' only, and is not a tool that we install per se, but should be
	# thought of as instead part of the build process.)

	local SOURCES=$1
	local BUILDDIR=$2

	(
	# Like with meson_compile, we do the work in a subshell so we can get much
	# of the environment, but not all of i

	module_purge

	for thing in gcc git git_lfs framel metaio gsl fftw hdf5 python3 ; do
		load_preinstalled $thing $PREFIX/python3_stuff
	done

	module load swig/4.0.2-python-3.7.4

	OLDPREFIX=$PREFIX
	PREFIX=$PREFIX/python3/
	# OK, now pip3

	# The --ignore-installed is needed (well, recommended) here because
	# otherwise we will pick up on the OzStar packages
	for pypkg in numpy astropy gobject PyGObject pyglib ligo.skymap h5py 'lscsoft-glue==1.59.2' ; do
		pip install --ignore-installed --prefix=$PREFIX $pypkg
	done

	PYTHONPATH=$PREFIX/lib/python3.7/site-packages/:$PYTHONPATH

	pushd $SOURCES/lalsuite
	./00boot
	configure_make_makeinstall $(pwd) $BUILDDIR/buildlalsuite-p3
	popd

	# lalsuite-extra not needed
	# TODO (true?)

	# Maybe not needed in subshell??
	PREFIX=$OLDPREFIX

	)

}

build_spiir_without_singularity() {
	local SPIIR_DIR=$1
	local DEPPREFIX=$2
	local BRANCH=$3
	local BRANCH_NAME=$4

	SPIIR_SOURCES="$SPIIR_DIR/$BRANCH_NAME/source"
	SPIIR_PREFIX="$SPIIR_DIR/$BRANCH_NAME/install"
	rm -rfv $SPIIR_PREFIX
	## Gstlal
	# This patch is a bit odd... I wrote it myself, because two includes seemed
	# to be missing from gstlal_simulation.c and it therefore wouldn't compile.
	# However we need to track down the actual source of the issue properly --
	# I guess probably just headers kept getting moved around.
	#apply_patch $SPIIR_SOURCES/gstlal gstlal_0000patrick_fix_includes.patch
	# We now have a new version of the patch to account for an extra newlne
	# introduced in revision 684bd84dfa in some branches. This new patch should
	# apply either way.
	apply_patch $SPIIR_SOURCES/gstlal gstlal_0001patrick_fix_includes_revised.patch

	# Going forward, we now need to be a bit careful about environment
	# variables, because the dependencies get installed in a different place
	# from gstlal, spiir etc. Essentially we adjust the (global!) PREFIX etc
	# variables (originally set in prepare_environment, and used by
	# configure_make_makeinstall and friends to configure things correctly) to
	# include the SPIIR stuff as well as the dependency stuff. However, we need
	# to be a bit careful about this because gstlal is special and needs hand
	# configured prefixes.
	# Probably to do all of these is a bit excessive (because I don't think
	# there are heavy dependencies between the gstlals) but we do them all for
	# safety

	if [[ "$USE_DEBUG" == "yes" ]]; then
		DEBUG_FLAGS="-fno-omit-frame-pointer -Og -gdwarf-2"
	fi

	PREFIX=$SPIIR_PREFIX # remember this is the *install* prefix
	PYTHONPATH=$SPIIR_PREFIX/lib/python2.7/site-packages:$PYTHONPATH
	BINPATH=$SPIIR_PREFIX/bin:$BINPATH
	PKG_CONFIG_PATH=$SPIIR_PREFIX/lib/pkgconfig:$SPIIR_PREFIX/lib64/pkgconfig:$PKG_CONFIG_PATH
	LD_LIBRARY_PATH=$SPIIR_PREFIX/lib:$SPIIR_PREFIX/lib64:$LD_LIBRARY_PATH
	# GI_TYPELIB_PATH is needed only on CIT (because we use the system
	# gobject-introspection) but I don't think it will hurt to include it on
	# OzSTAR
	GI_TYPELIB_PATH=$DEPPREFIX/share/gir-1.0/:${GI_TYPELIB_PATH:-}
	# It looks like we need to do GSTlal separately. The reason is that if it
	# picks up on the existence of gobject, it'll try to compile stuff with it.
	# But we need Python3 (installed as 'python3') for that to work, which we
	# don't have. So we need to not pass the lib64/ pkg-config path when
	# configuring gstlal
	pushd $SPIIR_SOURCES/gstlal
	# 00init.sh seems to return failure even when it succeeds
	yes | head -n1 | ./00init.sh || true
	# remember build_gstlal builds in the local directory now
	build_gstlal $(pwd) $DEPPREFIX $SPIIR_PREFIX
	popd

	for GSTLAL_THING in gstlal-inspiral gstlal-ugly ; do
		pushd $SPIIR_SOURCES/$GSTLAL_THING
		# 00init.sh seems to return failure even when it succeeds
		yes | head -n1 | ./00init.sh || true
		# We build in the source directory (a) so the user can efficiently
		# recompile, and (b) because gstlal-spiir won't work if built elsewhere
		configure_make_makeinstall_pip $(pwd) $(pwd)
		popd
	done

	# gstlal-spiir needs the extra --cuda=.... and also some extra crazy stuff
	# (a different PKG_CONFIG_PATH from normal because, bizarrely, it doesn't
	# seem to get automatically picked up on OzSTAR)
	local EXTRA_PKG_CONFIG_PATH=$PKG_CONFIG_PATH
	# This is a horrifying hack for OzStar. For whatever reason, 'module load
	# healpix' doesn't put the correct things in the PKG_CONFIG_PATH. So, if we
	# detect the presence of healpix's .pc in just the right place, we postpend
	# it to the PKG_CONFIG_PATH (postpend rather than prepend in case there is
	# already a healpix .pc already set up correctly).
	if [ -e /apps/skylake/software/compiler/gcc/6.4.0/healpix/3.50/lib/chealpix.pc ] ; then
		EXTRA_PKG_CONFIG_PATH=$EXTRA_PKG_CONFIG_PATH:/apps/skylake/software/compiler/gcc/6.4.0/healpix/3.50/lib
	fi

	local CUDA_LOC=$(find_cuda_location)
	pushd $SPIIR_SOURCES/gstlal-spiir
	yes | head -n1 | ./00init.sh || true
	CFLAGS=${DEBUG_FLAGS:-} PATH=$PREFIX_DEPENDENCIES/bin:$SYSTEM_PATH PYTHONPATH=$PYTHONPATH PKG_CONFIG_PATH=$EXTRA_PKG_CONFIG_PATH ./configure --prefix=$PREFIX --with-cuda=$CUDA_LOC
	PYTHONPATH=$PYTHONPATH PATH=$PREFIX_DEPENDENCIES/bin:$SYSTEM_PATH make -j$NUM_JOBS
	make install
	popd

	if [ -f "$SPIIR_SOURCES/gstlal-spiir/bin/trigger_control" ]; then
		pip install --prefix=$SPIIR_PREFIX redis
	fi
}

build_spiir_with_singularity() {
	load_singularity
	local INSTALL_DIR=$SPIIR_DIR/$BRANCH_NAME/install
	local SOURCE_DIR=$SPIIR_DIR/$BRANCH_NAME/source
	rm -rfv $INSTALL_DIR
	if [[ "$USE_DEBUG" == "yes" ]]; then
		local DEBUG_FLAGS=${DEBUG_FLAGS:-""}
		local SING_DEBUG_FLAGS="${DEBUG_FLAGS} -fPIC -g -O0 -ggdb -fno-omit-frame-pointer -rdynamic"
	fi
	local GSTLAL_PATCH=gstlal_0001patrick_fix_includes_revised.patch
	cat $(pwd)/$0 \
		| perl -ne "/^>>>>>$GSTLAL_PATCH/ ... /^>>>>>/ and print" | sed '1d;$d' \
		> $SOURCE_DIR/$GSTLAL_PATCH
	local CUDA_PATCH=e386300a.patch
	cat $(pwd)/$0 \
		| perl -ne "/^>>>>>$CUDA_PATCH/ ... /^>>>>>/ and print" | sed '1d;$d' \
		> $SOURCE_DIR/$CUDA_PATCH
	local OLD_CUDA_PATCH=pre-89d9ce8d.patch
	cat $(pwd)/$0 \
		| perl -ne "/^>>>>>$OLD_CUDA_PATCH/ ... /^>>>>>/ and print" | sed '1d;$d' \
		> $SOURCE_DIR/$OLD_CUDA_PATCH

	local REDIS_INSTALL=""
	if [ -f "$SOURCE_DIR/gstlal-spiir/bin/trigger_control" ]; then
		REDIS_INSTALL="pip install --prefix=$INSTALL_DIR redis"
	fi
	singularity exec --cleanenv --nv \
		$BIND_DIR \
		--env PATH="$INSTALL_DIR/bin:\$PATH" \
		--env PKG_CONFIG_PATH="$INSTALL_DIR/lib/pkgconfig:\$PKG_CONFIG_PATH" \
		--env GST_PLUGIN_PATH="$INSTALL_DIR/lib/gstreamer-0.10:\$GST_PLUGIN_PATH" \
		--env LD_LIBRARY_PATH="$INSTALL_DIR/lib:$INSTALL_DIR/lib64:\$LD_LIBRARY_PATH" \
		--env CFLAGS="\$CFLAGS ${SING_DEBUG_FLAGS:-}" \
		--env CXXFLAGS="\$CXXFLAGS ${SING_DEBUG_FLAGS:-}" \
		--env LDFLAGS="\$LDFLAGS ${SING_DEBUG_FLAGS:-}" \
		$SINGULARITY_BASE \
		bash -c "\
			pushd $SOURCE_DIR
			git apply --reject $OLD_CUDA_PATCH
			git apply --reject $CUDA_PATCH
			git apply $GSTLAL_PATCH
			pushd gstlal
			make distclean || true
			yes | head -n1 | ./00init.sh
			XDG_DATA_DIRS=$INSTALL_DIR/share:\${XDG_DATA_DIRS:-} ./configure --prefix=$INSTALL_DIR
			XDG_DATA_DIRS=$INSTALL_DIR/share:\${XDG_DATA_DIRS:-} make -j
			XDG_DATA_DIRS=$INSTALL_DIR/share:\${XDG_DATA_DIRS:-} make install -j
			popd
			pushd gstlal-inspiral
			make distclean || true
			yes | head -n1 | ./00init.sh
			./configure --prefix=$INSTALL_DIR
			make -j
			make install -j
			popd
			pushd gstlal-ugly
			make distclean || true
			yes | head -n1 | ./00init.sh
			./configure --prefix=$INSTALL_DIR
			make -j
			make install -j
			popd
			pushd gstlal-spiir
			NVCC_APPEND_FLAGS=\"-gencode=arch=compute_86,code=sm_86\"
			make distclean || true
			sed -i.original -e 's|.*\$(top_srcdir)/gnuscripts/cudalt.py.*|\t\$(top_srcdir)/gnuscripts/cudalt.py \$@ \$(NVCC) \$(NVCC_CFLAGS) \$(NVCC_ADD_CFLAGS) \$(DEFAULT_INCLUDES) \$(NVCC_LAL_CFLAGS) \$(NVCC_GSTLAL_CFLAGS) \$(NVCC_gstreamer_CFLAGS) \$(ADD_CFLAGS) --ptxas-options=-v -O0  -maxrregcount=0 -gencode=arch=compute_75,code=sm_75 -gencode=arch=compute_70,code=sm_70 -gencode=arch=compute_61,code=sm_61 -gencode=arch=compute_60,code=sm_60 -gencode=arch=compute_52,code=sm_52 --compiler-options=\\\\\"\$(libcuda_plugin_la_CFLAGS)\\\\\" -c $<|g' gst/cuda/Makefile.am
			yes | head -n1 | ./00init.sh
			export CFLAGS=\"\$CFLAGS -Wno-cast-function-type -Wno-unused-command-line-argument -Wno-unknown-warning-option -Wno-unused-function -Wno-implicit-fallthrough\"
			for FLAG in \$CFLAGS; do NVCC_APPEND_FLAGS=\"\$NVCC_APPEND_FLAGS -Xcompiler \$FLAG\"; done;
			./configure --prefix=$INSTALL_DIR --with-cuda=/usr/local/cuda
			make -j
			make install -j
			$REDIS_INSTALL"
}

build_spiir_py3_with_singularity() {
	load_singularity
	local INSTALL_DIR=$SPIIR_DIR/$BRANCH_NAME/install
	local SOURCE_DIR=$SPIIR_DIR/$BRANCH_NAME/source
	if [[ "$USE_DEBUG" == "yes" ]]; then
		local DEBUG_FLAGS=${DEBUG_FLAGS:-""}
		local SING_DEBUG_FLAGS="${DEBUG_FLAGS} -fPIC -g -O0 -ggdb -fno-omit-frame-pointer -rdynamic"
	fi
	local REDIS_INSTALL=""
	if [ -f "$SOURCE_DIR/gstlal-spiir/bin/trigger_control" ]; then
		REDIS_INSTALL="pip install --prefix=$INSTALL_DIR redis"
	fi
	singularity exec --cleanenv --nv \
		$BIND_DIR \
		--env PATH="$INSTALL_DIR/bin:\$PATH" \
		--env PKG_CONFIG_PATH="$INSTALL_DIR/lib/pkgconfig:\$PKG_CONFIG_PATH" \
		--env GST_PLUGIN_PATH="$INSTALL_DIR/lib/gstreamer-1.0:\$GST_PLUGIN_PATH" \
		--env LD_LIBRARY_PATH="$INSTALL_DIR/lib:$INSTALL_DIR/lib64:\$LD_LIBRARY_PATH" \
		--env CFLAGS="\$CFLAGS ${SING_DEBUG_FLAGS:-}" \
		--env CXXFLAGS="\$CXXFLAGS ${SING_DEBUG_FLAGS:-}" \
		--env LDFLAGS="\$LDFLAGS ${SING_DEBUG_FLAGS:-}" \
		$SINGULARITY_BASE \
		bash -c "\
			pushd $SOURCE_DIR
			pushd gstlal-spiir
			NVCC_APPEND_FLAGS=\"-gencode=arch=compute_86,code=sm_86\"
			make distclean || true
			yes | head -n1 | ./00init.sh
			export CFLAGS=\"\$CFLAGS -Wno-unknown-pragmas -Wno-sign-compare\"
			for FLAG in \$CFLAGS; do NVCC_APPEND_FLAGS=\"\$NVCC_APPEND_FLAGS -Xcompiler \$FLAG\"; done;
			./configure --prefix=$INSTALL_DIR --with-cuda=/usr/local/cuda
			make -j
			make install -j
			$REDIS_INSTALL"
}

find_cuda_location() {
	# We need to find where CUDA is installed (the gstlal-spiir configure
	# script wants the path, it doesn't use pkg-config or similar). For this,
	# we assume that nvcc is in a bin/ directory, and thus CUDA is installed
	# one level up
	if ! type nvcc &> /dev/null ; then
		echo "Couldn't find nvcc -> can't locate cuda install, please fix"
		echo "If you're on a cluster, check this is a GPU node?"
		echo " (e.g. on CIT -> pcdev11 is a good choice)"
		exit 1
	fi

	which nvcc | perl -ne '@a = split m@/@; print join "/", @a[0..$#a-2]'
}

tar_configure_make_makeinstall() {
	local TARBALL=$1
	local BUILD=$2

	mkdir -p $BUILD/src
	cp $TARBALL $BUILD/src/
	pushd $BUILD/src
	tar xvaf $(basename $TARBALL)
	popd

	TARDIR=$(basename ${TARBALL%.tar.*} )
	configure_make_makeinstall $BUILD/src/$TARDIR/ $BUILD/build "${@:3}"

}

configure_make_makeinstall() {
	local SRC=$1
	local BUILD=$2
	local EXTRA_CFLAGS=${3:-}

	mkdir -p $BUILD
	pushd $BUILD
	CFLAGS="$EXTRA_CFLAGS" PYTHONPATH=$PYTHONPATH PKG_CONFIG_PATH=$PKG_CONFIG_PATH $SRC/configure --prefix=$PREFIX "${@:4}"
	make -j$NUM_JOBS
	make install

	popd
}

# Same as the above, but with more environment variables to capture stuff
# that's been installed via pip and is needed during the configure/build
# process
configure_make_makeinstall_pip() {
	local SRC=$1
	local BUILD=$2
	local EXTRA_CFLAGS=${3:-}

	mkdir -p $BUILD
	pushd $BUILD
	PATH=$PREFIX_DEPENDENCIES/bin:$SYSTEM_PATH CFLAGS="${DEBUG_FLAGS:-} $EXTRA_CFLAGS" PYTHONPATH=$PYTHONPATH PKG_CONFIG_PATH=$PKG_CONFIG_PATH $SRC/configure --prefix=$PREFIX "${@:4}"
	PYTHONPATH=$PYTHONPATH PATH=$PREFIX_DEPENDENCIES/bin:$SYSTEM_PATH make -j$NUM_JOBS
	make install

	popd
}

tar_meson_compile() {
	# meson compilation is a bit of a pain, because meson is Python3-only, but
	# everything we're building is Python2-only. Thus, we need to do the
	# compilation in a subshell, where we've purged all the modules and then
	# reloaded just the key ones
	local TARBALL=$1
	local BUILD=$2

	mkdir -p $BUILD/src
	cp $TARBALL $BUILD/src
	pushd $BUILD/src
	tar xvaf $(basename $TARBALL)
	popd

	TARDIR=$(basename ${TARBALL%.tar.*} )
	meson_compile $BUILD/src/$TARDIR/
}

meson_compile() {
	# Note this is in a subshell
	# I also don't know how to do the meson build in a different directory, but
	# I think that's OK for now - particularly because we're extracting
	# everything to a temporary directory anyway
	local SRC=$1

	local PIP=pip
	if type pip3 &> /dev/null ; then
		# Prefer PIP3 if it exists -- this is because on CIT, pip is hardcoded
		# to talk to Python 2, and we'd prefer to talk to Python 3 (but on
		# OzSTAR there is no pip3)
		PIP=pip3
	fi

	(
		# Set up the environment in the subshell
		module_purge
		#for mod in python/3.7.4 cmake/3.12.4 ; do
		for thing in gcc python3 cmake ; do
			load_preinstalled $thing $PREFIX/python3_stuff
		done

		PYTHONPATH= $PIP install meson --prefix=$PREFIX/python3_stuff
		PYTHONPATH= $PIP install ninja --prefix=$PREFIX/python3_stuff

		# To my immense annoyance, CIT has Python 3.6 and OzStar has Python 3.7.
		# This means that when we install stuff to lib/pythonX.Y/site-packages, we
		# dont know in advance what X.Y is. (This is actually true of the Python 2
		# stuff, but thankfully 2.7 is a pretty safe guess.)
		local P3XY=$(PATH=$PREFIX/python3_stuff/bin:$PATH python3 -V 2>&1 | perl -ne '/(3\.\d+)\.\d/; print $1')

		# Note the different environment variables from normal
		# Note also that, for some bizarre reason, on OzStar everything gets
		# installed to lib (yay) but on CIT meson goes in lib and ninja goes in
		# lib64 (boo). Obviously we solve this by including both in the
		# PYTHONPATH
		pushd $SRC

		PATH=$PREFIX/python3_stuff/bin:$PATH PYTHONPATH=$PREFIX/python3_stuff/lib/python$P3XY/site-packages/:$PREFIX/python3_stuff/lib64/python$P3XY/site-packages PKG_CONFIG_PATH=$PKG_CONFIG_PATH $PREFIX/python3_stuff/bin/meson _build --prefix=$PREFIX
		PATH=$PREFIX/python3_stuff/bin:$PATH PYTHONPATH=$PREFIX/python3_stuff/lib/python$P3XY/site-packages/:$PREFIX/python3_stuff/lib64/python$P3XY/site-packages PKG_CONFIG_PATH=$PKG_CONFIG_PATH $PREFIX/python3_stuff/bin/ninja -v -C _build
		PATH=$PREFIX/python3_stuff/bin:$PATH PYTHONPATH=$PREFIX/python3_stuff/lib/python$P3XY/site-packages/:$PREFIX/python3_stuff/lib64/python$P3XY/site-packages PKG_CONFIG_PATH=$PKG_CONFIG_PATH $PREFIX/python3_stuff/bin/ninja -C _build install

		popd $SRC
	)
}

build_gstlal() {
	local SRC=$1
	local DEPPREFIX=$2
	local SPIIR_PREFIX=$3

	# Since this is the *very first* gstlal we install, it's probably overkill
	# to set up the PKG_CONFIG_PATH like this, but we do it anyway. Recall we
	# do this specially (rather than in configure_make_makeinstall) because
	# gstlal chokes on stuff in lib64/.
	XDG_DATA_DIRS=$DEPPREFIX/share:${XDG_DATA_DIRS:-} CFLAGS=${DEBUG_FLAGS:-} PYTHONPATH=$PYTHONPATH PKG_CONFIG_PATH=$SPIIR_PREFIX/lib/pkgconfig:$DEPPREFIX/lib/pkgconfig/:$SYSTEM_PKG_CONFIG_PATH $SRC/configure --prefix=$SPIIR_PREFIX
	XDG_DATA_DIRS=$DEPPREFIX/share:${XDG_DATA_DIRS:-} PKG_CONFIG_PATH=$PKG_CONFIG_PATH make -j$NUM_JOBS
	XDG_DATA_DIRS=$DEPPREFIX/share:${XDG_DATA_DIRS:-} make install
}

build_framecpp() {
	# This is a bit complicated, because for framecpp there's some weird
	# problem where 'make' fails during one of the tests(?). Essentially, for
	# that test, the build process overwrites PYTHONPATH (I think to point the
	# test script to the newly-built bits). Because of this, it can't find
	# critical components (like e.g. numpy). (I think this is a bug although
	# I'm not 100% sure.)
	# This means we need to apply a patch *after* configuring to the Makefile
	# itself.
	local TARBALL=$1
	local BUILD=$2
	local SCRIPT=$(pwd)/$0

	mkdir -p $BUILD/{src,build}
	cp $TARBALL $BUILD/src/
	pushd $BUILD/src
	tar xvaf $(basename $TARBALL)
	popd

	TARDIR=$BUILD/src/$(basename ${TARBALL%.tar.*} )

	pushd $BUILD/build
	PYTHONPATH=$PYTHONPATH PKG_CONFIG_PATH=$PKG_CONFIG_PATH $TARDIR/configure --prefix=$PREFIX
	pushd swig/python
	cat $SCRIPT \
	 | perl -ne "/^>>>>>framecpp_0000_Makefile_fix.patch/ ... /^>>>>>/ and print" | sed '1d;$d' \
	 | patch
	popd
	make -j$NUM_JOBS
	make install

	popd
}

apply_patch() {
	local DIR=$1
	local PATCH=$2

	local SCRIPT=$(pwd)/$0

	pushd $DIR

	# We copy the file out to the source tree and leave it there, as a reminder
	# that it's already been applied
	if [ ! -e "$PATCH" ] ; then
		echo "Apply patch $PATCH to $DIR"
		cat $SCRIPT \
		 | perl -ne "/^>>>>>$PATCH/ ... /^>>>>>/ and print" | sed '1d;$d' \
		 > $PATCH

		git apply $PATCH
	else
		echo "Patch $PATCH seems to be applied already"
	fi

	popd
}

git_clone() {
	local URL=$1
	local BRANCH=${2:-}
	local TARGET=${3:-}

	if [ -z $TARGET ] ; then
		TARGET=$(basename "$URL" .git)
	fi

	if [ -e "$TARGET" ] ; then
		echo "Would clone $URL but it looks like it's already been unpacked (to $TARGET)"
		return
	fi

	git clone "$URL" $TARGET
	if [ -n "$BRANCH" ] ; then
		pushd $TARGET
		git checkout $BRANCH
		popd
	fi
}

git_clone_single_branch() {
	local URL=$1
	local BRANCH=$2

	local TARGET=$(basename "$URL" .git)
	if [ -e "$TARGET" ] ; then
		echo "Would clone $URL but it looks like it's already been unpacked (to $TARGET)"
		return
	fi

	git clone --single-branch --branch $BRANCH "$URL" $TARGET
}

# load_preinstalled and friends are responsible for ensuring that all of the
# pre-installed software is in-place. By "pre-installed", we here mean stuff
# that this script is not capable of compiling, and which we expect the
# cluster/host to have already. (At some point we will support compiling more
# and more of this stuff, and mergining it in to the 'dependencies' command.)
#
# Complicating this, we want to support (as transparently as possible) both
# OzSTAR and CIT, and possibly other hosts. Therefore, load_module understands
# only a limited range of dependencies, each of which is (more or less)
# custom-loaded.
load_preinstalled() {
	THING=$1
	P3PREFIX=${2:-}
	PYTHON_OVERRIDE=${3:-}

	# For each case block, we first test if the thing we're looking for exists
	# already. If it does, we just fall through to the end of the subroutine.
	# If it doesn't, we try to load it in various ways (e.g. through 'module
	# load', if we're on a supercomputer cluster with lmod)
	case $THING in
		libxml2)
				try_modules libxml2/2.9.9
			;;
		git)
			load_git
			;;
		git_lfs)
			if ! git lfs &>/dev/null ; then
				try_modules_or_die git-lfs/2.4.0
			fi
			;;
		gcc)
			# Annoyingly even on OzStar, there is always a gcc in the path even
			# with no modules loaded. However not loading a gcc will confuse
			# various other things down the line, so we just unconditionally
			# (attempt) to load the relevant module
			try_modules gcc/6.4.0
			;;
		gsl)
			if ! pkg-config --exists gsl ; then
				try_modules_or_die gsl/2.5
			fi
			;;
		openmpi)
			if ! type mpirun &> /dev/null ; then
				# second module here is for CIT
				try_modules_or_die openmpi/3.0.0 mpi/openmpi-x86_64
			fi
			;;
		fftw)
			if ! pkg-config --exists fftw3f ; then
				try_modules_or_die fftw/3.3.7
			fi
			;;
		python2)
			if [ -n "$PYTHON_OVERRIDE" ] ; then
				# In this case we're asked to use the Python installation found
				# in PYTHON_OVERRIDE. We need to update PATH and PYTHONPATH
				# appropriately.
				# In a kinda-cheaty way, we export these
				# TODO: we export these because otherwise 'module' can't see
				# them and so will overwrite them. Think about whether this is
				# the right approach
				export PATH=$PYTHON_OVERRIDE/bin/:$PATH
				export PYTHONPATH=$PYTHON_OVERRIDE/lib/python2.7/site-packages/:${PYTHONPATH:-}
			elif [[ $(python --version 2>&1) == "Python 2."* ]] ; then
				# pass ;)
				# wonder if we should check other stuff here?
				true
			else
				try_modules_or_die python/2.7.14
			fi
			;;
		python3)
			# This is a tricky one. For OzStar, the choice is relatively easy:
			# module load python/2... or python/3... and then 'python' points
			# at the right thing. For CIT, it's harder: 'python' points at
			# Python 2, and 'python3' points at Python 3. So In that latter
			# case, we need to point 'python' to the correct place. The
			# cleanest way to do this seems to be to put a dummy Python
			# somewhere earlier in the path
			if [[ $(python --version 2>&1) == "Python 3."* ]] ; then
				# I guess nothing to do here, although now we can't load
				# Python2
				true
			elif type python3 &>/dev/null ; then
				# This is quite annoying. Thankfully we already have a special
				# path just for Python 3 things ($PREFIX/python3_stuff which
				# gets passed to us as $P3PREFIX) that we can use
				if [ ! -e $P3PREFIX/bin/python ] ; then
					mkdir -p $P3PREFIX/bin
					ln -s $(which python3) $P3PREFIX/bin/python
				fi
			else
				try_modules_or_die python/3.7.4
			fi

			;;
		numpy)
			if ! python -c 'import numpy' &> /dev/null ; then
				try_modules_or_die numpy/1.14.1-python-2.7.14
			fi
			;;
		scipy)
			if ! python -c 'import scipy' &> /dev/null ; then
				try_modules_or_die scipy/1.0.0-python-2.7.14
			fi
			;;
		swig-python)
			try_modules swig/3.0.12-python-2.7.14
			;;
		framel)
			if ! pkg-config --exists libframe ; then
				try_modules_or_die framel/8.30
			fi
			;;
		metaio)
			if ! pkg-config --exists libmetaio ; then
				try_modules_or_die metaio/8.4.0
			fi
			;;
		hdf5)
			try_modules hdf5/1.10.1-serial
			;;
		matplotlib-python)
			if ! python -c 'import matplotlib' &> /dev/null ; then
				try_modules_or_die matplotlib/2.2.2-python-2.7.14
			fi
			;;
		cmake)
			if ! type cmake &> /dev/null ; then
				try_modules_or_die cmake/3.12.4
			fi
			;;
		cuda)
			# On CIT GPU nodes, /usr/local/cuda points to the latest cuda. So
			# if that exists, we can just hand-set PATH, LD_LIBRARY_PATH
			if [ -e /usr/local/cuda-11.2/bin/nvcc ] ; then
				PATH=/usr/local/cuda-11.2/bin:$PATH
				LD_LIBRARY_PATH=/usr/local/cuda-11.2/lib64:${LD_LIBRARY_PATH:-}
			elif ! type nvcc &> /dev/null ; then
				try_modules_or_die cuda/10.0.130
			fi

			if ! type nvcc &> /dev/null ; then
				# Some strange issue on CIT seems to result in us not dying if
				# we can't find the module?
				echo "Couldn't load nvcc, bailing out"
				echo "If you're on a cluster, check this is a GPU node?"
				echo " (e.g. on CIT -> pcdev11 is a good choice)"
				exit 1
			fi
			;;
		healpix)
			if ! echo "#include <chealpix.h>" | gcc -M -E - &> /dev/null ; then
				try_modules_or_die healpix/3.50
			fi
			;;
		lapack)
			if ! echo 'int main() {}' | gcc -x c -o /dev/null -llapack - &> /dev/null ; then
				try_modules_or_die lapack/3.8.0
			fi
			;;
		*)
			echo "Internal error - can't install $THING?"
			exit 1
			;;
	esac
}

# load_git -- helper for git specifically (because in a few places we load it
# specially)
load_git() {
	if ! type git &>/dev/null ; then
		try_modules_or_die git/2.18.0
	fi
}

# load_singularity -- helper for singularity specifically
load_singularity() {
	if ! type singularity &>/dev/null ; then
		try_modules_or_die apptainer apptainer/latest
	fi
}

# module_purge -- run 'module purge' iff lmod is installed
module_purge() {
	if type module &> /dev/null ; then
		module purge &> /dev/null
	fi
}

# Try to 'module load' the list of modules. If no modules exist (or if the
# 'module' command doesn't exist), complain to the user and exit
try_modules_or_die() {
	THING=$1
	if ! type module &>/dev/null ; then
		echo "Couldn't figure out how to load $THING, sorry"
		exit 1
	fi

	while [ -n "$1" ] ; do
		if module load $1 ; then
			return
		fi
		shift
	done

	echo "Couldn't figure out how to load $THING, sorry"
	exit 1
}

# Try to 'module load' a list of modules; and if it doesn't work that's fine
# (there are two modules that need to be loaded on OzStar but not CIT. I don't
# know why they're not needed on CIT so I can't write tests, so we just try to
# load them blindly.)
try_modules() {
	THING=$1
	if ! type module &>/dev/null ; then
		return
	fi

	while [ -n "$1" ] ; do
		if module load $1 ; then
			return
		fi
		shift
	done
}


# defaults
DONT_CLEANUP=${DONT_CLEANUP:-0}

# and go
main "$@"

exit

# Below this we store the patches... probably not an ideal place to put them,
# but I guess it is what it is
>>>>>lalsuite_0000_cleanup.patch
commit 236c8a51a0195418c51555fc7c90b3df514c96b1
Author: Joel Bosveld <joel.bosveld@uwa.edu.au>
Date:   Fri Oct 5 16:09:29 2018 +1000

    LALInspiralIIR: clean code

diff --git a/lalinspiral/lib/LALInspiralIIR.c b/lalinspiral/lib/LALInspiralIIR.c
index 954385644a..fdbb8e70fa 100644
--- a/lalinspiral/lib/LALInspiralIIR.c
+++ b/lalinspiral/lib/LALInspiralIIR.c
@@ -1,5 +1,9 @@
 /*
 
+  Copyright (C) 2010 Shaun Hooper
+  Copyright (C) 2012-2014 Dave McKenzie, Qi Chu
+  Copyright (C) 2015 Dave McKenzie, Yan Wang
+
   This code relates to Infinite Impulse Response filters that correspond to an
   inspiral waveform.  The idea is that a sum a set of delayed first order IIR
   filters with one feedback coefficient (a1) and one feedforward (b0)
@@ -12,7 +16,6 @@
   To generate the IIR set of a1's, b0's and delays, you need to provide a
   amplitude and phase time series.
 
-  Created by Shaun Hooper 2010-05-28
 
 */
 
@@ -39,12 +42,14 @@ static REAL8 clogabs(COMPLEX16 z)
 
 int XLALInspiralGenerateIIRSet(REAL8Vector *amp, REAL8Vector *phase, double epsilon, double alpha, double beta, double padding, COMPLEX16Vector **a1, COMPLEX16Vector **b0, INT4Vector **delay)
 {  
-	int j = amp->length-1, jstep, jstepThird, k;
-	int nfilters = 0, decimationFactor = 1;
-	double phase_tdot, phase_ddot, phase_dot;
+	int j = amp->length-1, jstep, k, nfilters = 0;
+	REAL8 phase_tdot, phase_ddot, phase_dot, jstep_third, jstep_second;
+
+	padding += 0.0;
+	if (padding < 0.0) {
+		printf("warning: padding value %f is not supported yet ", padding);
+	}
 
-	//printf("This is confirming that the LALInspiralIIR.c code has been successfully modified");
-	/* FIXME: Add error checking for lengths of amp and phase */
 	if (amp->length != phase->length) 
 	         XLAL_ERROR(XLAL_EINVAL);
 
@@ -52,63 +57,60 @@ int XLALInspiralGenerateIIRSet(REAL8Vector *amp, REAL8Vector *phase, double epsi
 	*b0 = XLALCreateCOMPLEX16Vector(0);
 	*delay = XLALCreateINT4Vector(0);
 	
-	//printf("This is the modified code\n");
 
 	while (j > 3 ) {
-		//int prej = j;
-		/* Reset j so that the delay will be an integar number of decimated rate */
-		//j = amp->length-1 - (int) floor((amp->length-1-j)/(double) decimationFactor + 0.5)*decimationFactor;
 
-		/* Get second derivative term */
-		phase_ddot = (phase->data[j-2] - 2.0 * phase->data[j-1] + phase->data[j]) / (2.0 * LAL_PI);
-		phase_tdot = (phase->data[j-3] - 3.0 * phase->data[j-2] + 3.0 * phase->data[j-1] - phase->data[j]) / (2.0*LAL_PI);
+		/* Get derivative terms */
+		if (j  > (int) (phase->length - 3) ) {
+			phase_ddot = (phase->data[j-2] - 2.0 * phase->data[j-1] + phase->data[j]) / LAL_TWOPI;
+			phase_tdot = (phase->data[j-3] - 3.0 * phase->data[j-2] + 3.0 * phase->data[j-1] - phase->data[j]) / LAL_TWOPI;
+		}
+		else {
+			phase_ddot = (phase->data[j-1] - 2.0 * phase->data[j] + phase->data[j+1]) / LAL_TWOPI;
+			phase_tdot = ( -0.5 * phase->data[j-2] + phase->data[j-1] - phase->data[j+1] + 0.5 * phase->data[j+2]) / LAL_TWOPI;
+		}
+
+
 		phase_ddot = fabs(phase_ddot);
 		phase_tdot = fabs(phase_tdot);
 
-		if (phase_ddot < 0 || phase_ddot > 8*epsilon){
-				j = j - 1;
-			continue;
+		jstep_second = floor(sqrt(2.0 * epsilon / phase_ddot) + 0.5);
+		jstep_third = floor(pow(6.0 * epsilon / phase_tdot, 1./3) + 0.5);
+		// FIXME: check int_max > jstep*
+		if(jstep_third > jstep_second){
+			jstep = (int) jstep_second;
 		}
-
-		jstep = (int) fabs(floor(sqrt(2.0 * epsilon / fabs(phase_ddot))+0.5));
-		jstepThird = (int) fabs(floor(pow(6.0 * epsilon / fabs(phase_tdot),1./3)+0.5));
-		jstep = abs(jstep);
-		jstepThird = abs(jstepThird);
-
-		//jstepThird integer overflow??
-		if(jstep > jstepThird && jstepThird >0){
-		    jstep = jstepThird;
+		else {
+			jstep = (int) jstep_third;
 		}
-		if(jstep == 0){
-		    jstep = 1;
+
+		if(jstep < 2){
+		    jstep = 2;
 		}
-		k = (int ) floor((double ) j - alpha * (double ) jstep + 0.5);
+
+		k = (int) floor((double ) j - alpha * (double ) jstep + 0.5);
+
 		if (k < 1){
 		    jstep = j;
 		    k = (int ) floor((double ) j - alpha * (double ) jstep + 0.5);
 		}
-		//printf("jstep: %d jstepThird: %d k: %d j:%d \n ", jstep, jstepThird, k, j);
 		
+		nfilters++;
 
-		if(k == 0){
-		    k = 1;
+		if (k < 0) {
+			XLAL_ERROR(XLAL_EINVAL);
 		}
 
-
-		if (k <= 2) break;
-		nfilters++;
-
 		if (k > (int) amp->length-3) {
 			phase_dot = (11.0/6.0*phase->data[k] - 3.0*phase->data[k-1] + 1.5*phase->data[k-2] - 1.0/3.0*phase->data[k-3]);
 		}
+		else if (k >= 2 ) {
+			phase_dot = (-phase->data[k+2] + 8.0 * (phase->data[k+1] - phase->data[k-1]) + phase->data[k-2]) / 12.0; // Five-point stencil first derivative of phase
+		}
 		else {
-			phase_dot = (-phase->data[k+2] + 8 * (phase->data[k+1] - phase->data[k-1]) + phase->data[k-2]) / 12.0; // Five-point stencil first derivative of phase
+			phase_dot = (-11.0/6.0*phase->data[k] + 3.0*phase->data[k+1] - 1.5*phase->data[k+2] + 1.0/3.0*phase->data[k+3]);
 		}
-		//fprintf(stderr, "%3.0d, %6.0d, %3.0d, %11.2f, %11.8f\n",nfilters, amp->length-1-j, decimationFactor, ((double) (amp->length-1-j))/((double) decimationFactor), phase_dot/(2.0*LAL_PI)*2048.0);
-		decimationFactor = ((int ) pow(2.0,-ceil(log(2.0*padding*phase_dot/(2.0*LAL_PI))/log(2.0))));
-		if (decimationFactor < 1 ) decimationFactor = 1;
 
-		//fprintf(stderr, "filter = %d, prej = %d, j = %d, k=%d, jstep = %d, decimation rate = %d, nFreq = %e, phase[k] = %e\n", nfilters, prej, j, k, jstep, decimationFactor, phase_dot/(2.0*LAL_PI), phase->data[k]);
 		/* FIXME: Should think about being smarter about allocating memory for these (linked list??) */
 		*a1 = XLALResizeCOMPLEX16Vector(*a1, nfilters);
 		*b0 = XLALResizeCOMPLEX16Vector(*b0, nfilters);
@@ -120,8 +122,8 @@ int XLALInspiralGenerateIIRSet(REAL8Vector *amp, REAL8Vector *phase, double epsi
 		(*delay)->data[nfilters-1] = amp->length - 1 - j;
 
 
+		if (k < 2) break;
 
-		/* Calculate the next data point step */
 		j -= jstep;
 
 	}
@@ -148,10 +150,9 @@ int XLALInspiralIIRSetResponse(COMPLEX16Vector *a1, COMPLEX16Vector *b0, INT4Vec
 	for (a1_last = a1f + numFilters; a1f < a1_last; a1f++)
 		{
 			y = *b0f / *a1f;
-			length = (int) (logf(1e-13))/(logf(cabs(*a1f)));// + *delayf;
-			//length = (int) (logf((1e-13)/cabs(*b0f)))/(logf(cabs(*a1f)));// + *delayf;
+			length = (int) (log(1e-13)/log(cabs(*a1f)));
 			int maxlength = response->length - *delayf;
-			if (length > maxlength)
+			if (length > maxlength || length < 0)
 				length = maxlength;
 
 			complex double *response_data = (complex double *) &response->data[*delayf];
>>>>>lalsuite_0001_variable_epsilon.patch
commit 600ac31384fd51b172a27c1c2318ea3c30115118
Author: Joel Bosveld <joel.bosveld@uwa.edu.au>
Date:   Mon Oct 8 12:45:32 2018 +1100

    LALInspiralIIR: change epsilon based on amp to improve delays

diff --git a/lalinspiral/lib/LALInspiralIIR.c b/lalinspiral/lib/LALInspiralIIR.c
index fdbb8e70fa..d331b24add 100644
--- a/lalinspiral/lib/LALInspiralIIR.c
+++ b/lalinspiral/lib/LALInspiralIIR.c
@@ -40,10 +40,30 @@ static REAL8 clogabs(COMPLEX16 z)
   return log(max) + 0.5 * log1p(u * u);
 }
 
-int XLALInspiralGenerateIIRSet(REAL8Vector *amp, REAL8Vector *phase, double epsilon, double alpha, double beta, double padding, COMPLEX16Vector **a1, COMPLEX16Vector **b0, INT4Vector **delay)
-{  
+int XLALInspiralGenerateIIRSet(REAL8Vector *amp, REAL8Vector *phase, double epsilon_in, double alpha, double beta, double padding, COMPLEX16Vector **a1, COMPLEX16Vector **b0, INT4Vector **delay)
+{
+        // Parameters to be changed for testing
+        float cutoff = 0.05;
+        float power = 0.5;
+        float increase_eps = 100;
+        float margin = 10;
+
 	int j = amp->length-1, jstep, k, nfilters = 0;
 	REAL8 phase_tdot, phase_ddot, phase_dot, jstep_third, jstep_second;
+        REAL8 maxamp=0;
+        unsigned int argmaxamp=0;
+        for(unsigned int i=0;i<amp->length;i++)
+          if(amp->data[i]>maxamp)
+            {
+            maxamp=amp->data[i];
+            argmaxamp=i;
+            }
+        for(unsigned int i=amp->length-1;i>argmaxamp;i--)
+          if(amp->data[i]>cutoff*maxamp)
+            {
+              argmaxamp=i;
+            break;
+            }
 
 	padding += 0.0;
 	if (padding < 0.0) {
@@ -74,6 +94,9 @@ int XLALInspiralGenerateIIRSet(REAL8Vector *amp, REAL8Vector *phase, double epsi
 		phase_ddot = fabs(phase_ddot);
 		phase_tdot = fabs(phase_tdot);
 
+                double epsilon = epsilon_in * pow((maxamp / amp->data[j]), power);
+                if(j>(int)argmaxamp)
+                  epsilon *= increase_eps;
 		jstep_second = floor(sqrt(2.0 * epsilon / phase_ddot) + 0.5);
 		jstep_third = floor(pow(6.0 * epsilon / phase_tdot, 1./3) + 0.5);
 		// FIXME: check int_max > jstep*
@@ -123,8 +146,11 @@ int XLALInspiralGenerateIIRSet(REAL8Vector *amp, REAL8Vector *phase, double epsi
 
 
 		if (k < 2) break;
-
-		j -= jstep;
+                /* Calculate the next data point step */
+                if(j>(int)argmaxamp+margin && j-jstep<(int)argmaxamp-margin)
+                  j=argmaxamp;
+		else
+		  j -= jstep;
 
 	}
 
>>>>>lalsuite_0002_libxml_header.patch
diff --git a/gstlal-spiir/gst/cuda/cohfar/cohfar_accumbackground.h b/gstlal-spiir/gst/cuda/cohfar/cohfar_accumbackground.h
index 1b5070f1..c3480659 100644
--- a/gstlal-spiir/gst/cuda/cohfar/cohfar_accumbackground.h
+++ b/gstlal-spiir/gst/cuda/cohfar/cohfar_accumbackground.h
@@ -33,6 +33,7 @@
 #include <gst/base/gstbasetransform.h>
 #include <gst/gst.h>
 #include <postcohtable.h>
+#include <libxml/xmlwriter.h>
 
 G_BEGIN_DECLS
 #define COHFAR_ACCUMBACKGROUND_TYPE (cohfar_accumbackground_get_type())

>>>>>glue_0000_zipsafe.patch
diff --git a/setup.py b/setup.py
index f4ee55d1..6da7b650 100644
--- a/setup.py
+++ b/setup.py
@@ -111,6 +111,7 @@ setup(
   license = 'See file LICENSE',
   packages = [ 'glue', 'glue.ligolw', 'glue.ligolw.utils', 'glue.segmentdb', 'glue.auth'],
   install_requires=install_requires,
+  zip_safe = False,
   cmdclass = {
     'build_py' : glue_build_py,
     'clean' : glue_clean,
>>>>>gstlal_0000patrick_fix_includes.patch
From 38a232082b41d3ecb40917ed12ae1ccd721c21c1 Mon Sep 17 00:00:00 2001
From: Patrick Clearwater <patrick@clearwater.id.au>
Date: Thu, 12 Dec 2019 12:11:30 +1100
Subject: [PATCH] Add extra headers to gstlal_simulation.c

(It won't compile without these #includes, at least not on OzSTAR.)
---
 gstlal/gst/lal/gstlal_simulation.c | 3 ++-
 1 file changed, 2 insertions(+), 1 deletion(-)

diff --git a/gstlal/gst/lal/gstlal_simulation.c b/gstlal/gst/lal/gstlal_simulation.c
index 0df15d7e2..42d052ed5 100644
--- a/gstlal/gst/lal/gstlal_simulation.c
+++ b/gstlal/gst/lal/gstlal_simulation.c
@@ -84,7 +84,8 @@
  * our own stuff
  */
 
-
+#include <lal/LIGOMetadataUtils.h>
+#include <lal/LIGOMetadataInspiralUtils.h>
 #include <gstlal/gstlal.h>
 #include <gstlal/gstlal_tags.h>
 #include <gstlal_simulation.h>
-- 
2.17.1

>>>>>gstlal_0001patrick_fix_includes_revised.patch
From 728d3df47273772381b0aea57d55aa74ea6f8582 Mon Sep 17 00:00:00 2001
From: Patrick Clearwater <pclearwater@swinburne.edu.au>
Date: Wed, 22 Apr 2020 15:18:36 +1000
Subject: [PATCH] Fix includes in gstlal_simulation.c (new version).

SPIIR won't compile on OzStar without this extra patch.

This is a new version to work around the extra newline introduced in
684bd84dfa.
---
 gstlal/gst/lal/gstlal_simulation.c | 2 ++
 1 file changed, 2 insertions(+)

diff --git a/gstlal/gst/lal/gstlal_simulation.c b/gstlal/gst/lal/gstlal_simulation.c
index 025a1176..c45abe8e 100644
--- a/gstlal/gst/lal/gstlal_simulation.c
+++ b/gstlal/gst/lal/gstlal_simulation.c
@@ -89,6 +89,8 @@
 #include <gstlal/gstlal.h>
 #include <gstlal/gstlal_tags.h>
 #include <gstlal_simulation.h>
+#include <lal/LIGOMetadataUtils.h>
+#include <lal/LIGOMetadataInspiralUtils.h>
 
 
 /*
-- 
2.18.0

>>>>>framecpp_0000_Makefile_fix.patch
--- Makefile	2019-12-12 13:24:08.448227206 +1100
+++ Makefile	2019-12-06 15:25:10.151096744 +1100
@@ -1439,7 +1439,7 @@
 frameCPP_python.stamp: hdr-time-stamp
 
 Z-Sample_Python_ver8-600000000-1.gwf: test_frameCPP.py _frameCPP.la
-	env PYTHONPATH=$(LDAS_DIRECTORY_BUILD_LIB_FRAMECPP)/swig/python/.libs:$(LDAS_DIRECTORY_BUILD_LIB_FRAMECPP)/swig/python \
+	env PYTHONPATH=$(LDAS_DIRECTORY_BUILD_LIB_FRAMECPP)/swig/python/.libs:$(LDAS_DIRECTORY_BUILD_LIB_FRAMECPP)/swig/python:${PYTHONPATH} \
 	$(OS_LIBRARY_PATH)=$(FRAMESPEC_LIB_PATHS):$(LDASTOOLSAL_LIBDIRS):${$(OS_LIBRARY_PATH)}	 \
 	$(PYTHON) $(srcdir)/test_frameCPP.py
 
>>>>>manoj_00_gstreamer.patch
diff --git a/gst/gststructure.c b/gst/gststructure.c
index fe54305c1..310380451 100644
--- a/gst/gststructure.c
+++ b/gst/gststructure.c
@@ -763,9 +763,9 @@ gst_structure_set_field (GstStructure * structure, GstStructureField * field)
         g_value_unset (&field->value);
         return;
       } else {
-        /* empty strings never make sense */
+        /* empty strings never make sense *
         GIT_G_WARNING ("Trying to set empty string on taglist field '%s'. "
-            "Please file a bug.", g_quark_to_string (field->name));
+            "Please file a bug.", g_quark_to_string (field->name)); */
         g_value_unset (&field->value);
         return;
       }

>>>>>e386300a.patch
diff --git a/gstlal-spiir/gst/cuda/postcoh/postcoh_kernel.cu b/gstlal-spiir/gst/cuda/postcoh/postcoh_kernel.cu
index 4a4dc87a9..93530a1b4 100644
--- a/gstlal-spiir/gst/cuda/postcoh/postcoh_kernel.cu
+++ b/gstlal-spiir/gst/cuda/postcoh/postcoh_kernel.cu
@@ -36,6 +36,7 @@ const int GAMMA_ITMAX = 50;
 #define WARP_SIZE     32
 #define WARP_MASK     31
 #define LOG_WARP_SIZE 5
+#define ALL_THREADS_MASK 0xFFFFFFFF
 
 #define MIN_EPSILON       1e-7
 #define MAXIFOS           6
@@ -99,9 +100,11 @@ __global__ void ker_max_snglsnr(COMPLEX_F **snr, // INPUT: snr
         // inter-warp reduction to find the max snr among threads in the warp
         for (int j = WARP_SIZE / 2; j > 0; j = j >> 1) {
 
-            temp_snr_sq = __shfl(max_snr_sq, wIDl + j, 2 * j);
+            temp_snr_sq =
+              __shfl_sync(ALL_THREADS_MASK, max_snr_sq, wIDl + j, 2 * j);
 
-            temp_template = __shfl(max_template, wIDl + j, 2 * j);
+            temp_template =
+              __shfl_sync(ALL_THREADS_MASK, max_template, wIDl + j, 2 * j);
             max_template  = (max_template + temp_template)
                            + (max_template - temp_template)
                                * (2 * (max_snr_sq > temp_snr_sq) - 1);
@@ -420,11 +423,11 @@ __global__ void ker_coh_max_and_chisq_versatile(
     volatile int *restrict sky_idx_shared      = (int *)&nullstream_shared[wn];
 
     // float    *mu;    // matrix u for certain sky direction
-    int peak_cur, tmplt_cur, ipeak_max = 0;
+    int peak_cur, tmplt_cur;
     COMPLEX_F dk[MAXIFOS];
     int NtOff;
     int map_idx;
-    float real, imag, factor;
+    float real, imag;
     float al_all = 0.0f, chisq_cur;
 
     float stat_max, stat_tmp;
@@ -503,10 +506,11 @@ __global__ void ker_coh_max_and_chisq_versatile(
         }
 
         for (i = WARP_SIZE / 2; i > 0; i = i >> 1) {
-            stat_tmp           = __shfl_xor(stat_max, i);
-            snr_tmp            = __shfl_xor(snr_max, i);
-            nullstream_max_tmp = __shfl_xor(nullstream_max, i);
-            sky_idx_tmp        = __shfl_xor(sky_idx, i);
+            stat_tmp = __shfl_xor_sync(ALL_THREADS_MASK, stat_max, i);
+            snr_tmp  = __shfl_xor_sync(ALL_THREADS_MASK, snr_max, i);
+            nullstream_max_tmp =
+              __shfl_xor_sync(ALL_THREADS_MASK, nullstream_max, i);
+            sky_idx_tmp = __shfl_xor_sync(ALL_THREADS_MASK, sky_idx, i);
 
             if (stat_tmp > stat_max) {
                 stat_max       = stat_tmp;
@@ -592,14 +596,15 @@ __global__ void ker_coh_max_and_chisq_versatile(
                 laneChi2 += (data.re * data.re + data.im * data.im);
             }
             for (int k = WARP_SIZE >> 1; k > 0; k = k >> 1) {
-                laneChi2 += __shfl_xor(laneChi2, k, WARP_SIZE);
+                laneChi2 +=
+                  __shfl_xor_sync(ALL_THREADS_MASK, laneChi2, k, WARP_SIZE);
             }
             if (srcLane == 0) { snr_shared[wID] = laneChi2; }
             __syncthreads();
             if (threadIdx.x < wn) {
                 laneChi2 = snr_shared[srcLane];
                 for (i = wn / 2; i > 0; i = i >> 1) {
-                    laneChi2 += __shfl_xor(laneChi2, i);
+                    laneChi2 += __shfl_xor_sync(ALL_THREADS_MASK, laneChi2, i);
                 }
                 if (srcLane == 0) {
                     chisq_cur = laneChi2 / autocorr_norm[j][tmplt_cur];
@@ -625,7 +630,7 @@ __global__ void ker_coh_max_and_chisq_versatile(
          *
          */
 
-        int ipix = 0, rand_range = trial_sample_inv * hist_trials - 1;
+        int ipix = 0;
         for (itrial = 1 + threadIdx.x / WARP_SIZE; itrial <= hist_trials;
              itrial += blockDim.x / WARP_SIZE) {
             snr_max        = 0.0;
@@ -633,6 +638,7 @@ __global__ void ker_coh_max_and_chisq_versatile(
             sky_idx        = 0;
 
             // FIXME: try using random offset like the following
+            // rand_range = trial_sample_inv * hist_trials - 1;
             // trial_offset = rand()% rand_range + 1;
             trial_offset  = itrial * trial_sample_inv;
             output_offset = peak_cur + (itrial - 1) * max_npeak;
@@ -699,10 +705,11 @@ __global__ void ker_coh_max_and_chisq_versatile(
             }
 
             for (i = WARP_SIZE / 2; i > 0; i = i >> 1) {
-                stat_tmp           = __shfl_xor(stat_max, i);
-                snr_tmp            = __shfl_xor(snr_max, i);
-                nullstream_max_tmp = __shfl_xor(nullstream_max, i);
-                sky_idx_tmp        = __shfl_xor(sky_idx, i);
+                stat_tmp = __shfl_xor_sync(ALL_THREADS_MASK, stat_max, i);
+                snr_tmp  = __shfl_xor_sync(ALL_THREADS_MASK, snr_max, i);
+                nullstream_max_tmp =
+                  __shfl_xor_sync(ALL_THREADS_MASK, nullstream_max, i);
+                sky_idx_tmp = __shfl_xor_sync(ALL_THREADS_MASK, sky_idx, i);
 
                 if (stat_tmp > stat_max) {
                     stat_max       = stat_tmp;
@@ -789,7 +796,8 @@ __global__ void ker_coh_max_and_chisq_versatile(
                     laneChi2 += (data.re * data.re + data.im * data.im);
                 }
                 for (int k = WARP_SIZE >> 1; k > 0; k = k >> 1) {
-                    laneChi2 += __shfl_xor(laneChi2, k, WARP_SIZE);
+                    laneChi2 +=
+                      __shfl_xor_sync(ALL_THREADS_MASK, laneChi2, k, WARP_SIZE);
                 }
 
                 if (srcLane == 0) {
@@ -906,9 +914,6 @@ void cohsnr_and_chisq(PostcohState *state,
                       int gps_idx,
                       int output_skymap,
                       cudaStream_t stream) {
-    size_t freemem;
-    size_t totalmem;
-
     int threads = 256;
     int sharedsize =
       MAX(2 * threads * sizeof(float), 4 * threads / WARP_SIZE * sizeof(float));
diff --git a/gstlal-spiir/gst/cuda/multiratespiir/multiratespiir_kernel.cu b/gstlal-spiir/gst/cuda/multiratespiir/multiratespiir_kernel.cu
index dc1afb53a..7b0c4df0e 100644
--- a/gstlal-spiir/gst/cuda/multiratespiir/multiratespiir_kernel.cu
+++ b/gstlal-spiir/gst/cuda/multiratespiir/multiratespiir_kernel.cu
@@ -39,25 +39,10 @@ extern "C" {
 
 #define THREADSPERBLOCK 256
 #define NB_MAX          32
+#define ALL_THREADS_MASK 0xFFFFFFFF
 
 //#define ORIGINAL
 #define CUT_FILTERS 0 // set to 0 to keep all the filtering results
-#if 0
-// deprecated: we have cuda_debug.h for gpu debug now
-#define gpuErrchk(stream)                                                      \
-    { gpuAssert(stream, __FILE__, __LINE__); }
-static void gpuAssert(cudaStream_t stream, char *file, int line, bool abort=true)
-{
-	cudaStreamSynchronize(stream);
-	cudaError_t code = cudaPeekAtLastError ();
-
-	if (code != cudaSuccess) 
-	{
-		printf ("GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
-		if (abort) exit(code);
-	}
-}
-#endif
 extern __shared__ char sharedMem[];
 
 __global__ void downsample2x(const float amplifier,
@@ -241,8 +226,10 @@ __global__ void
 
         // Inter-Warp Reduction
         for (int off = WARPSIZE >> 1; off > 0; off = off >> 1) {
-            fltrOutptReal += __shfl_down(fltrOutptReal, off, 2 * off);
-            fltrOutptImag += __shfl_down(fltrOutptImag, off, 2 * off);
+            fltrOutptReal +=
+              __shfl_down_sync(ALL_THREADS_MASK, fltrOutptReal, off, 2 * off);
+            fltrOutptImag +=
+              __shfl_down_sync(ALL_THREADS_MASK, fltrOutptImag, off, 2 * off);
         }
 
         if (tLane == 0) {
@@ -266,8 +253,8 @@ __global__ void
                                : 0;
 
             for (int off = WARPSIZE >> 1; off > 0; off = off >> 1) {
-                sum_real += __shfl_down(sum_real, off);
-                sum_imag += __shfl_down(sum_imag, off);
+                sum_real += __shfl_down_sync(ALL_THREADS_MASK, sum_real, off);
+                sum_imag += __shfl_down_sync(ALL_THREADS_MASK, sum_imag, off);
             }
 
             if (threadIdx.x == 0) {
@@ -360,8 +347,10 @@ __global__ void cuda_iir_filter_kernel_fine(COMPLEX_F *cudaA1,
 
             // Inter-CU Reduction
             for (int off = cu >> 1; off > 0; off = off >> 1) {
-                fltrOutptReal += __shfl(fltrOutptReal, tLane + off, 2 * off);
-                fltrOutptImag += __shfl(fltrOutptImag, tLane + off, 2 * off);
+                fltrOutptReal += __shfl_sync(ALL_THREADS_MASK, fltrOutptReal,
+                                             tLane + off, 2 * off);
+                fltrOutptImag += __shfl_sync(ALL_THREADS_MASK, fltrOutptImag,
+                                             tLane + off, 2 * off);
             }
 
             if (tIdL == 0) {
@@ -502,7 +491,7 @@ __global__ void outdata_reshape(
     unsigned int tx = threadIdx.x, tdx = blockDim.x;
     unsigned int by = blockIdx.y, channels = gridDim.y;
     int mem_out_start = mem_out_len * by + filt_len - 1;
-    float *in, *out;
+    float *out;
     int i;
 
     for (i = tx; i < len; i += tdx) {
@@ -885,7 +874,6 @@ gint spiirup(SpiirState **spstate,
             // Use Fine-Grained Kernel
             int numTemplates = SPSTATE(i)->num_templates;
             int numFilters   = SPSTATE(i)->num_filters;
-            int mem_len      = SPSTATEUP(i)->mem_len;
             int t            = numFilters - 1;
             int c            = 0;
             while (t >>= 1) { ++c; }
@@ -994,7 +982,6 @@ gint spiirup(SpiirState **spstate,
                 // Use Fine-Grained Kernel
                 int numTemplates = SPSTATE(i)->num_templates;
                 int numFilters   = SPSTATE(i)->num_filters;
-                int mem_len      = SPSTATEUP(i)->mem_len;
                 int t            = numFilters - 1;
                 int c            = 0;
                 while (t >>= 1) { ++c; }
>>>>>pre-89d9ce8d.patch
diff --git a/gstlal-spiir/gst/cuda/multiratespiir/multiratespiir_kernel.cu b/gstlal-spiir/gst/cuda/multiratespiir/multiratespiir_kernel.cu
index 3938df22b..352af4de4 100644
--- a/gstlal-spiir/gst/cuda/multiratespiir/multiratespiir_kernel.cu
+++ b/gstlal-spiir/gst/cuda/multiratespiir/multiratespiir_kernel.cu
@@ -43,6 +43,7 @@ extern "C" {
 
 #define THREADSPERBLOCK 256
 #define NB_MAX 32
+#define ALL_THREADS_MASK 0xFFFFFFFF
 
 //#define ORIGINAL
 #define CUT_FILTERS 0 // set to 0 to keep all the filtering results
@@ -254,8 +255,8 @@ __global__ void cuda_iir_filter_kernel_coarse
 		// Inter-Warp Reduction
 		for (int off = WARPSIZE >> 1; off > 0; off = off >> 1)
 		{
-			fltrOutptReal += __shfl_down(fltrOutptReal, off, 2 * off);
-			fltrOutptImag += __shfl_down(fltrOutptImag, off, 2 * off);
+			fltrOutptReal += __shfl_down_sync(ALL_THREADS_MASK, fltrOutptReal, off, 2 * off);
+			fltrOutptImag += __shfl_down_sync(ALL_THREADS_MASK, fltrOutptImag, off, 2 * off);
 		}
 
 		if (tLane == 0)
@@ -268,6 +269,7 @@ __global__ void cuda_iir_filter_kernel_coarse
 		// Wait for all partial sums to be done
 		__syncthreads();
 
+		unsigned warp_mask = __ballot_sync(0xFFFFFFFF, tWarp == 0);
 		// Warp-reduce partial sums to get final sum
 		if (tWarp == 0) {
 			// Load each partial sum (if that warp existed) into a lane-local value in warp 0
@@ -275,8 +277,8 @@ __global__ void cuda_iir_filter_kernel_coarse
 			float sum_imag = (threadIdx.x < blockDim.x >> LOGWARPSIZE) ? partialSumsImag[tLane] : 0;
 
 			for (int off = WARPSIZE >> 1; off > 0; off = off >> 1) {
-				sum_real += __shfl_down(sum_real, off);
-				sum_imag += __shfl_down(sum_imag, off);
+				sum_real += __shfl_down_sync(warp_mask, sum_real, off);
+				sum_imag += __shfl_down_sync(warp_mask, sum_imag, off);
 			}
 
 			if (threadIdx.x == 0) {
@@ -344,6 +346,8 @@ __global__ void cuda_iir_filter_kernel_fine
 	fltrOutptReal = 0.0f;
 	fltrOutptImag = 0.0f;
 
+        unsigned compute_unit_mask = __ballot_sync(0xFFFFFFFF, cuIdG < numTemplates);
+
 	// Total number of CUs should exceed numTemplates
 	if (cuIdG < numTemplates)
 	{
@@ -371,8 +375,8 @@ __global__ void cuda_iir_filter_kernel_fine
 			// Inter-CU Reduction
 			for (int off = cu >> 1; off > 0; off = off >> 1)
 			{
-				fltrOutptReal += __shfl(fltrOutptReal, tLane + off, 2 * off);
-				fltrOutptImag += __shfl(fltrOutptImag, tLane + off, 2 * off);
+				fltrOutptReal += __shfl_sync(compute_unit_mask, fltrOutptReal, tLane + off, 2 * off);
+				fltrOutptImag += __shfl_sync(compute_unit_mask, fltrOutptImag, tLane + off, 2 * off);
 			}
 
 			if (tIdL == 0)
diff --git a/gstlal-spiir/gst/cuda/postcoh/postcoh_kernel.cu b/gstlal-spiir/gst/cuda/postcoh/postcoh_kernel.cu
index c770199df..7a619e730 100644
--- a/gstlal-spiir/gst/cuda/postcoh/postcoh_kernel.cu
+++ b/gstlal-spiir/gst/cuda/postcoh/postcoh_kernel.cu
@@ -36,6 +36,7 @@ const int GAMMA_ITMAX = 50;
 #define WARP_SIZE 		32
 #define WARP_MASK		31
 #define LOG_WARP_SIZE	5
+#define ALL_THREADS_MASK 0xFFFFFFFF
 
 #define MIN_EPSILON 1e-7
 #define MAXIFOS 6
@@ -96,9 +97,9 @@ __global__ void ker_max_snglsnr
         for (int j = WARP_SIZE / 2; j > 0; j = j >> 1)
         {
 
-            temp_snr_sq    = __shfl(max_snr_sq , wIDl + j, 2 * j);
+            temp_snr_sq    = __shfl_sync(ALL_THREADS_MASK, max_snr_sq , wIDl + j, 2 * j);
 
-            temp_template = __shfl(max_template, wIDl + j, 2 * j); 
+            temp_template = __shfl_sync(ALL_THREADS_MASK, max_template, wIDl + j, 2 * j); 
             max_template = (max_template + temp_template) + (max_template - temp_template) * (2 * (max_snr_sq > temp_snr_sq) - 1);
             max_template = max_template >> 1;
 
@@ -503,10 +504,10 @@ __global__ void ker_coh_max_and_chisq_versatile
 
         for (i = WARP_SIZE / 2; i > 0; i = i >> 1)
         {
-            stat_tmp = __shfl_xor(stat_max, i);
-            snr_tmp = __shfl_xor(snr_max, i);
-            nullstream_max_tmp = __shfl_xor(nullstream_max, i);
-            sky_idx_tmp = __shfl_xor(sky_idx, i);
+            stat_tmp = __shfl_xor_sync(ALL_THREADS_MASK, stat_max, i);
+            snr_tmp = __shfl_xor_sync(ALL_THREADS_MASK, snr_max, i);
+            nullstream_max_tmp = __shfl_xor_sync(ALL_THREADS_MASK, nullstream_max, i);
+            sky_idx_tmp = __shfl_xor_sync(ALL_THREADS_MASK, sky_idx, i);
 
             if (stat_tmp > stat_max)
             {
@@ -589,16 +590,17 @@ __global__ void ker_coh_max_and_chisq_versatile
             }
             for (int k = WARP_SIZE >> 1; k > 0; k = k >> 1)
             {
-                laneChi2 += __shfl_xor(laneChi2, k, WARP_SIZE);
+                laneChi2 += __shfl_xor_sync(ALL_THREADS_MASK, laneChi2, k, WARP_SIZE);
             }
             if (srcLane == 0) {
                 snr_shared[wID] = laneChi2; 
             }
+	    unsigned chisq_mask = __ballot_sync(0xFFFFFFFF, threadIdx.x < wn);
             __syncthreads();
             if (threadIdx.x < wn) {
                 laneChi2 = snr_shared[srcLane];
                 for (i = wn / 2; i > 0; i = i >> 1) {
-                    laneChi2 += __shfl_xor(laneChi2, i);
+                    laneChi2 += __shfl_xor_sync(chisq_mask, laneChi2, i);
                 }
                 if (srcLane == 0)
                 {
@@ -687,10 +689,10 @@ __global__ void ker_coh_max_and_chisq_versatile
 
         for (i = WARP_SIZE / 2; i > 0; i = i >> 1)
         {
-            stat_tmp = __shfl_xor(stat_max, i);
-            snr_tmp = __shfl_xor(snr_max, i);
-            nullstream_max_tmp = __shfl_xor(nullstream_max, i);
-            sky_idx_tmp = __shfl_xor(sky_idx, i);
+            stat_tmp = __shfl_xor_sync(ALL_THREADS_MASK, stat_max, i);
+            snr_tmp = __shfl_xor_sync(ALL_THREADS_MASK, snr_max, i);
+            nullstream_max_tmp = __shfl_xor_sync(ALL_THREADS_MASK, nullstream_max, i);
+            sky_idx_tmp = __shfl_xor_sync(ALL_THREADS_MASK, sky_idx, i);
 
             if (stat_tmp > stat_max)
             {
@@ -761,7 +763,7 @@ __global__ void ker_coh_max_and_chisq_versatile
             }
             for (int k = WARP_SIZE >> 1; k > 0; k = k >> 1)
             {
-                laneChi2 += __shfl_xor(laneChi2, k, WARP_SIZE);
+                laneChi2 += __shfl_xor_sync(ALL_THREADS_MASK, laneChi2, k, WARP_SIZE);
             }
 
             if (srcLane == 0)
@@ -908,10 +910,10 @@ __global__ void ker_coh_max_and_chisq
 
         for (i = WARP_SIZE / 2; i > 0; i = i >> 1)
         {
-            stat_tmp = __shfl_xor(stat_max, i);
-            snr_tmp = __shfl_xor(snr_max, i);
-            nullstream_max_tmp = __shfl_xor(nullstream_max, i);
-            sky_idx_tmp = __shfl_xor(sky_idx, i);
+            stat_tmp = __shfl_xor_sync(ALL_THREADS_MASK, stat_max, i);
+            snr_tmp = __shfl_xor_sync(ALL_THREADS_MASK, snr_max, i);
+            nullstream_max_tmp = __shfl_xor_sync(ALL_THREADS_MASK, nullstream_max, i);
+            sky_idx_tmp = __shfl_xor_sync(ALL_THREADS_MASK, sky_idx, i);
 
             if (stat_tmp > stat_max)
             {
@@ -995,16 +997,17 @@ __global__ void ker_coh_max_and_chisq
             }
             for (int k = WARP_SIZE >> 1; k > 0; k = k >> 1)
             {
-                laneChi2 += __shfl_xor(laneChi2, k, WARP_SIZE);
+                laneChi2 += __shfl_xor_sync(ALL_THREADS_MASK, laneChi2, k, WARP_SIZE);
             }
             if (srcLane == 0) {
                 snr_shared[wID] = laneChi2; 
             }
+	    unsigned chisq_mask = __ballot_sync(0xFFFFFFFF, threadIdx.x < wn);
             __syncthreads();
             if (threadIdx.x < wn) {
                 laneChi2 = snr_shared[srcLane];
                 for (i = wn / 2; i > 0; i = i >> 1) {
-                    laneChi2 += __shfl_xor(laneChi2, i);
+                    laneChi2 += __shfl_xor_sync(chisq_mask, laneChi2, i);
                 }
                 if (srcLane == 0)
                 {
@@ -1083,10 +1086,10 @@ __global__ void ker_coh_max_and_chisq
 
         for (i = WARP_SIZE / 2; i > 0; i = i >> 1)
         {
-            stat_tmp = __shfl_xor(stat_max, i);
-            snr_tmp = __shfl_xor(snr_max, i);
-            nullstream_max_tmp = __shfl_xor(nullstream_max, i);
-            sky_idx_tmp = __shfl_xor(sky_idx, i);
+            stat_tmp = __shfl_xor_sync(ALL_THREADS_MASK, stat_max, i);
+            snr_tmp = __shfl_xor_sync(ALL_THREADS_MASK, snr_max, i);
+            nullstream_max_tmp = __shfl_xor_sync(ALL_THREADS_MASK, nullstream_max, i);
+            sky_idx_tmp = __shfl_xor_sync(ALL_THREADS_MASK, sky_idx, i);
 
             if (stat_tmp > stat_max)
             {
@@ -1157,7 +1160,7 @@ __global__ void ker_coh_max_and_chisq
             }
             for (int k = WARP_SIZE >> 1; k > 0; k = k >> 1)
             {
-                laneChi2 += __shfl_xor(laneChi2, k, WARP_SIZE);
+                laneChi2 += __shfl_xor_sync(ALL_THREADS_MASK, laneChi2, k, WARP_SIZE);
             }
 
             if (srcLane == 0)
>>>>>end
